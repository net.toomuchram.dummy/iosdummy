#!/bin/bash

if [ -d /tmp/iosdummy ]; then
    rm -rf /tmp/iosdummy
fi
cd /tmp

echo "> Cloning GitLab repo"
echo "$ git clone https://gitlab.com/net.toomuchram.dummy/iosdummy"
git clone https://gitlab.com/net.toomuchram.dummy/iosdummy
echo "$ cd iosdummy"
cd iosdummy
echo " "

echo "> Deleting git repo"
echo "$ rm -rf .git"
rm -rf .git
echo " "

echo "> Removing Pods"
echo "$ rm -rf Pods"
rm -rf Pods
echo " "

echo "> Creating .travis.yml"
echo "wget https://gist.githubusercontent.com/TooMuchRAM/fa0680a255032e0c80b4e4722203f086/raw/d2889065d0a5d7268c5cdde40e4f9100ddd3e0b8/.travis.yml"
wget https://gist.githubusercontent.com/TooMuchRAM/fa0680a255032e0c80b4e4722203f086/raw/d2889065d0a5d7268c5cdde40e4f9100ddd3e0b8/.travis.yml
echo " "

echo "> Initialising new git repo"
echo "$ git init"
git init
echo "$ git add ."
git add .
echo "$ git commit -m \"Automated commit\""
git commit -m "Automated commit"

echo " "
echo "> Pushing to GitHub"
echo "$ git remote add origin https://github.com/TooMuchRAM/iosdummy"
git remote add origin https://github.com/TooMuchRAM/iosdummy
echo "$ git push -u origin master --force"
git push -u origin master --force