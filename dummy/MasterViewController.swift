//
//  MasterViewController.swift
//  dummy
//
//  Created by TooMuchRAM on 30/07/2019.
//  Copyright © 2019 TooMuchRAM. All rights reserved.
//

import UIKit
import UserNotifications
import CoreLocation
import SwiftLocation

class MasterViewController: UITableViewController, CLLocationManagerDelegate {

    var sections: [String: Array<(String, String)>] = [:]
    var sectionsActionMapper: [String: [String]] = [:]
    var sectionTitles: [String] = Array()

    var detailViewController: DetailViewController? = nil
    var locationManager: CLLocationManager!

    var showSuggestedNote: Bool = true


    override func viewDidLoad() {
        super.viewDidLoad()
        

        print()
        
        // Do any additional setup after loading the view.
        
        let preferences = UserDefaults.standard
        let admin = preferences.bool(forKey: "admin")


        // MARK: Menu
        //Format:
        //"section name": [("section member", "section member type")]
        sections = [
            "Algemeen": [("Info", "Info"), ("Geotracker", "Geotracker"), ("Groepen", "Teams"), ("Programma", "Programma"), ("Foto's", "Photoboard"), ("Notificaties", "Notificaties"), ("Chat", "Chat"), ("Audio", "Audio")],
            "Tijdlijn": [("Tijdlijn deel 1", "Tijdlijn"), ("Tijdlijn deel 2", "Tijdlijn")],
            "Les 0: inleiding": [("Locatie Rome", "Aantekening"), ("Krantenartikel", "Huiswerk")],
            "Les 1: het ontstaan van Rome": [("Aeneas' vlucht", "Aantekening"), ("Rhea Silvia en Mars", "Aantekening"), ("Romulus en Remus", "Aantekening"), ("Sabijnse Maagdenroof", "Aantekening"), ("Romulus' Hutje", "Huiswerk"), ("Horatii & Curiatii", "Huiswerk"), ("Stamboom 1", "Stamboom"), ("Stamboom 2", "Stamboom")],
            "Les 2: de koningstijd in Rome": [("Lucretia", "Aantekening"), ("Romeinse koningen", "Aantekening"), ("Strip Lucretia", "Huiswerk")],
            "Les 3: de Republiek": [("Begin Republiek", "Aantekening"), ("Eerste Punische Oorlog", "Aantekening"), ("Tweede Punische Oorlog", "Aantekening"), ("Derde Punische Oorlog", "Aantekening"), ("Veroveringen Republiek", "Aantekening"), ("Veroveringen Republiek", "Kaart"), ("Informatievoorziening", "Huiswerk"), ("Interview", "Huiswerk")],
            "Les 4: de Republiek (vervolg)": [("Proletariërs en Nobiles", "Aantekening")],
            "Les 5: de Republiek (Caesar)": [("Marius vs Sulla", "Aantekening"), ("Caesar vs Pompeius", "Aantekening"), ("Interview", "Huiswerk")],
            "Les 6: gebouwen": [("Gebouwen Koningstijd", "Aantekening"), ("Gebouwen Republiek", "Aantekening"), ("Roddelartikel Vergilia", "Huiswerk")],
            "Les 7: Octavianus": [("Octavianus", "Aantekening"), ("Na Caesar's dood", "Aantekening"), ("Octavianus vs Antonius", "Aantekening")],
            "Les 8: Augustus": [("Augustus", "Aantekening"), ("Propaganda", "Aantekening"), ("Gebouwen Caesar", "Aantekening"), ("Gebouwen Augustus", "Aantekening"), ("Reisverslag", "Huiswerk")],
            "Les 9: Het Julisch-Claudische Huis": [("Tiberius", "Aantekening"), ("Caligula", "Aantekening"), ("Claudius", "Aantekening"), ("Nero", "Aantekening"), ("Stamboom 3", "Stamboom"), ("Keizers", "Stamboom")],
            "Les 10: Nero": [("Grote Brand van Rome", "Aantekening"), ("Domus Aurea", "Aantekening"), ("Muurschilderingen", "Aantekening"), ("Brand in Rome", "Huiswerk")],
            "Les 11: De Flaviërs": [("Vespasianus", "Aantekening"), ("Titus", "Aantekening"), ("Domitianus", "Aantekening"), ("Colosseum", "Aantekening")],
            "Les 12: Trajanus": [("Trajanus", "Aantekening"), ("Gebouwen Trajanus", "Aantekening"), ("Zuil van Trajanus", "Aantekening")],
            "Les 13: Hadrianus": [("Hadrianus", "Aantekening"), ("Gebouwen Hadrianus", "Aantekening"), ("Pantheon - hemellichamen", "Aantekening"), ("Pius, Aurelius en Commodus", "Aantekening")],
            "Rome Voorbereidingsweek": [("De Romeinse Godsdienst", "Aantekening"), ("Cultus van Mithras", "Aantekening"), ("Tauroktonie", "Aantekening"), ("Begin Christendom", "Aantekening"), ("Het vroege Christendom", "Aantekening"), ("Christelijke symbolen", "Aantekening"), ("Het latere Christendom", "Aantekening"), ("La Roma dei Santi", "Aantekening"), ("Eenwording Italië", "Aantekening"), ("VIR", "Huiswerk")],
            "Les 14: Diocletianus en Constantijn": [("Septimius Severus", "Aantekening"), ("Caracalla", "Aantekening"), ("Diocletianus", "Aantekening"), ("Thermen van Diocletianus", "Aantekening"), ("Constantijn de Grote", "Aantekening"), ("Boog van Constantijn 1", "Aantekening"), ("Boog van Constantijn 2", "Aantekening")],
            "Les 15: Gebouwen uit de Keizertijd": [("Forum Romanum (Keizertijd)", "Aantekening"), ("Atrium Vestae", "Gebouw"), ("Basilica Aemilia", "Gebouw"), ("Basilica Julia", "Gebouw"), ("Curia Julia", "Gebouw"), ("Regia", "Gebouw"), ("Rostra Augusti", "Gebouw"), ("Tabularium", "Gebouw"), ("Tempel van Divus Julius", "Gebouw"), ("Tempel van Saturnus", "Gebouw"), ("Tempel van Vesta", "Gebouw")],
            "Les 16: de Renaissance": [("De Renaissance", "Aantekening"), ("Pausen in de Renaissance", "Aantekening"), ("Pausen I", "Aantekening"), ("Kunstenaars I", "Aantekening"), ("School van Athene", "Aantekening"), ("Plafond Sixtijnse Kapel", "Aantekening")],
            "Les 17: de Barok": [("Barok", "Aantekening"), ("Pausen II", "Aantekening"), ("Kunstenaars II", "Aantekening"), ("Verhaal Baldakijn", "Huiswerk")],
            "Grieks": [("Boukefalas", "Huiswerk"), ("De vrouwen rond Darius", "Huiswerk"), ("Roxane", "Huiswerk"), ("Grafplaat", "Huiswerk")],
            "Overig": [("Privacy Policy", "Privacy"), ("Over", "Over")]
        ]
        if admin {
            sections["Admin"] = [("Verzend aankondiging", "VerzendAankondiging"), ("Denktank Chat", "Chat")]
        }

        sectionsActionMapper = [
            "Algemeen": ["special:info", "special:geotracker", "file:teams.html", "file:programme.html", "special:photoboard", "special:feed", "special:chat", "special:audio"],
            "Tijdlijn": ["file:tijdlijn1.png", "file:tijdlijn2.png"],
            "Les 0: inleiding": ["file:les0/locatierome.html", "file:les0/krantenartikel.html"],
            "Les 1: het ontstaan van Rome": ["file:les1/aeneasvlucht.html", "file:les1/rheasilviamars.html", "file:les1/romulusremus.html", "file:les1/sabijnsemaagdenroof.html", "file:les1/romulushutje.png", "file:les1/horatiicuriatii.png", "file:les1/stamboom0.png", "file:les1/stamboom1.png"],
            "Les 2: de koningstijd in Rome": ["file:les2/lucretia.html", "file:les2/koningen.html", "file:les2/striplucretia.png"],
            "Les 3: de Republiek": ["file:les3/beginrepubliek.html", "file:les3/punischeoorlog1.html", "file:les3/punischeoorlog2.html", "file:les3/punischeoorlog3.html", "file:les3/veroveringenrepubliek.html", "file:les3/veroveringenrepubliek.png", "file:les3/informatievoorziening.html", "file:les3/interview.html"],
            "Les 4: de Republiek (vervolg)": ["file:les4/proletariersnobiles.html"],
            "Les 5: de Republiek (Caesar)": ["file:les5/mariussulla.html", "file:les5/caesarpompeius.html", "file:les5/interview.html"],
            "Les 6: gebouwen": ["file:les6/gebouwenkoningstijd.html", "file:les6/gebouwenrepubliek.html", "file:les6/roddelartikel.html", ],
            "Les 7: Octavianus": ["file:les7/octavianus.html", "file:les7/nacaesarsdood.html", "file:les7/octavianusantonius.html"],
            "Les 8: Augustus": ["file:les8/augustus.html", "file:les8/propaganda.html", "file:les8/gebouwencaesar.html", "file:les8/gebouwenaugustus.html", "file:les8/reisverslag.html"],
            "Les 9: Het Julisch-Claudische Huis": ["file:les9/tiberius.html", "file:les9/caligula.html", "file:les9/claudius.html", "file:les9/nero.html", "file:les9/stamboom.png", "file:les9/keizers.png"],
            "Les 10: Nero": ["file:les10/grotebrand.html", "file:les10/domusaurea.html", "file:les10/muurschilderingen.html", "file:les10/de_grote_brand.jpg"],
            "Les 11: De Flaviërs": ["file:les11/vespasianus.html", "file:les11/titus.html", "file:les11/domitianus.html", "file:les11/colosseum.html"],
            "Les 12: Trajanus": ["file:les12/trajanus.html", "file:les12/gebouwentrajanus.html", "file:les12/zuil.png"],
            "Les 13: Hadrianus": ["file:les13/hadrianus.html", "file:les13/gebouwenhadrianus.html", "file:les13/pantheon-hemellichamen.png", "file:les13/pius-aurelius-commodus.html"],
            "Rome Voorbereidingsweek": ["file:rvw/romeinsegodsdienst.html", "file:rvw/cultusmithras.html", "file:rvw/tauroktonie.png", "file:rvw/beginchristendom.html", "file:rvw/vroegechristendom.html", "file:rvw/christelijkesymbolen.html", "file:rvw/laterechristendom.html", "file:rvw/laromadeisanti.png", "file:rvw/eenwordingitalie.html", "file:rvw/vir.html"],
            "Les 14: Diocletianus en Constantijn": ["file:les14/severus.html", "file:les14/caracalla.html", "file:les14/diocletianus.html", "file:les14/thermendiocletianus.html", "file:les14/constantijndegrotefaggot.html", "file:les14/boogconstantijn.png", "file:les14/boogconstantijn.html"],
            "Les 15: Gebouwen uit de Keizertijd": ["file:les15/forum-romanum-der-kaiserzeit.png", "file:les15/atrium_vestae.jpg", "file:les15/basilica_aemilia.jpg", "file:les15/basilica_julia.jpg", "file:les15/curia_julia.jpg", "file:les15/regia.png", "file:les15/rostra_augusti.jpg", "file:les15/tabularium.jpg", "file:les15/tempel_van_divus_julius.jpg", "file:les15/tempel_van_saturnus.jpg", "file:les15/tempel_van_vesta.jpg"],
            "Les 16: de Renaissance": ["file:les16/renaissance.html", "file:les16/pausenrenaissance.html", "file:les16/pausen-I.html", "file:les16/kunstenaars-I.html", "file:les16/school-van-athene.png", "file:les16/plafond-sixtijnse-kapel.png"],
            "Les 17: de Barok": ["file:les17/barok.html", "file:les17/pausen-II.html", "file:les17/kunstenaars-II.html", "file:les17/verhaal.html"],
            "Grieks": ["file:grieks/alexander-boukefalos.html", "file:grieks/alexander-vrouwen-rond-dareios.html", "file:grieks/alexander-roxane.html", "file:grieks/grafplaat.html"],
            "Overig": ["special:privacypolicy", "file:about.html"]
        ]
        if admin {
            sectionsActionMapper["Admin"] = ["special:verzendaankondiging", "special:adminchat"]
        }

        sectionTitles = ["Algemeen", "Tijdlijn", "Les 0: inleiding", "Les 1: het ontstaan van Rome", "Les 2: de koningstijd in Rome", "Les 3: de Republiek", "Les 4: de Republiek (vervolg)", "Les 5: de Republiek (Caesar)", "Les 6: gebouwen", "Les 7: Octavianus", "Les 8: Augustus", "Les 9: Het Julisch-Claudische Huis", "Les 10: Nero", "Les 11: De Flaviërs", "Les 12: Trajanus", "Les 13: Hadrianus", "Rome Voorbereidingsweek", "Les 14: Diocletianus en Constantijn", "Les 15: Gebouwen uit de Keizertijd", "Les 16: de Renaissance", "Les 17: de Barok", "Grieks", "Overig"]
        if admin {
            sectionTitles.insert("Admin", at: 1)
        }

        if let split = splitViewController {
            let controllers = split.viewControllers
            detailViewController = (controllers[controllers.count - 1] as! UINavigationController).topViewController as? DetailViewController
        }
        
        self.navigationController?.navigationBar.prefersLargeTitles = true
        self.navigationController?.navigationItem.largeTitleDisplayMode = .always

        //Check if the user is within the specified region
        // "Middle" because this is in the middle of all lower markers
        let middle0 = CLLocation(latitude: 41.892205, longitude: 12.4825395)
        let middle1 = CLLocation(latitude: 41.90602, longitude: 12.47642)

        let radius0 = 3500.0 // in metres
        let radius1 = 95.0


        LocationManager.shared.locateFromGPS(.oneShot, accuracy: .block) { result in
            switch result {
            case .failure(let error):
                debugPrint("Received error: \(error)")
            case .success(let location): do {
                if location.distance(from: middle0) < radius0 || location.distance(from: middle1) < radius1 {
                    self.showSuggestedNote = false
                    let storyboard = UIStoryboard(name: "Main", bundle: nil)
                    let controller = storyboard.instantiateViewController(withIdentifier: "TextViewController") as! TextViewController
                    controller.detailItem = "file:gebouwen_suggestie.html"
                    controller.modalPresentationStyle = .pageSheet
                    self.present(controller, animated: true, completion: nil)
                }
            }
            }
        }

    }

    override func viewDidAppear(_ animated: Bool) {
        //check if the user is logged in
        let preferences = UserDefaults.standard

        if preferences.object(forKey: "sessionId") == nil {
            //load login screen
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let controller = storyboard.instantiateViewController(withIdentifier: "LoginController")
            controller.modalPresentationStyle = .fullScreen //or .overFullScreen for transparency
            self.present(controller, animated: true, completion: nil)
        } else {
            AppUtilities().requestNotifPerms()
        }
    }


    override func viewWillAppear(_ animated: Bool) {
        clearsSelectionOnViewWillAppear = splitViewController!.isCollapsed
        super.viewWillAppear(animated)
    }

    // MARK: - Segues

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "showDetail" {
            if let indexPath = tableView.indexPathForSelectedRow {

                let sectionTitle = sectionTitles[indexPath.section]
                let sectionActions = sectionsActionMapper[sectionTitle]
                let sectionMembers = sections[sectionTitle]

                let controller = (segue.destination as! UINavigationController).topViewController as! DetailViewController
                controller.detailItem = sectionActions![indexPath.row]
                controller.detailItemTitle = sectionMembers![indexPath.row].0
                controller.navigationItem.leftBarButtonItem = splitViewController?.displayModeButtonItem
                controller.navigationItem.leftItemsSupplementBackButton = true

                let backItem = UIBarButtonItem()
                backItem.title = "Terug"
                navigationItem.backBarButtonItem = backItem // This will show in the next view controller being pushed
            }
        }
    }

    // MARK: - Table View

    override func numberOfSections(in tableView: UITableView) -> Int {
        return sections.count
    }

    override func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        //This function wants the title of a particular section.
        //Solution: Look it up in the sectionTitles array.
        return sectionTitles[section]
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        //This function wants the number of rows in a particular section.
        //Problem: Dictionaries are unordered, so you cannot get the right section by index
        //Solution: Look up the title first in the sectionTitles array, then use that title to get the needed section
        let sectionTitle = sectionTitles[section]
        let sectionMembers = sections[sectionTitle]
        return sectionMembers!.count
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath)
        //This function wants the title of a particular item within a section
        //Problem: Dictionaries are unordered, so you cannot get the right section by index
        //Solution: Look up the title first in the sectionTitles array, then use that title to get the needed section
        //          and use the provided index within the section to get the title

        let sectionTitle = sectionTitles[indexPath.section]
        let sectionMembers = sections[sectionTitle]
        let type = sectionMembers![indexPath.row].1

        cell.textLabel!.text = sectionMembers![indexPath.row].0
        //sectionMembers![indexPath.row].1 holds the section member type
        //the section member type is the same as the section member type imagename
        cell.imageView!.image = UIImage(named: type)
        //cell.textLabel!.highlightedTextColor = .lightGray
        //cell.selectedBackgroundView?.tintColor = .lightGray
        //cell.selectedBackgroundView?.backgroundColor = .lightGray
        return cell
    }
}

