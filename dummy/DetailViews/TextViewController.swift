//
//  TextViewController.swift
//  dummy
//
//  Created by TooMuchRAM on 26/09/2019.
//  Copyright © 2019 TooMuchRAM. All rights reserved.
//

import Foundation
import UIKit
class TextViewController: UIViewController {
    
    // MARK: properties
    @IBOutlet weak var maintextview: UITextView!
    @IBOutlet weak var loadinganimation: UIActivityIndicatorView!
    
    private let appUtils = AppUtilities()
    private let utils = TextViewControllerUtilities()
    
    var detailItem: String? {
        didSet {
            // Update the view.
            //configureView()
        }
    }
    
    
    // MARK: configureView
    func configureView(){
        let action = detailItem!.split(separator: ":")[0]
        if action == "file" {
            let fullfilename = detailItem!.split(separator: ":")[1]
            
            if fullfilename == "programme.html" || fullfilename == "info.html" || fullfilename == "teams.html" {
                // Because the programme is imported from the Augustus server, it must be loaded from disk
                // The AppDelegate has already fetched the latest version and saved it to disk
                // but download the latest version anyway lol it's like 1000 bytes
                let preferences = UserDefaults.standard
                let sessionId = String(preferences.string(forKey: "sessionId") ?? "")
                if fullfilename == "programme.html" {
                    appUtils.getAndSaveProgramme(sessionId: sessionId)
                } else if fullfilename == "teams.html" {
                    appUtils.getAndSaveTeams(sessionId: sessionId)
                }
                let html = appUtils.openHTMLfromDisk(name: String(fullfilename)) ?? ""
                let correctedhtml = html.replacingOccurrences(of: "<body>", with: "<body style=\"font: -apple-system-body\">")
                loadHTML(html: correctedhtml)
            }
            else {
                let fileext = NSString(string: String(fullfilename)).pathExtension
                let filename = NSString(string: NSString(string: String(fullfilename)).lastPathComponent).deletingPathExtension
                let filedir = NSString(string: String(fullfilename)).deletingLastPathComponent
                
                
                let filePath = appUtils.lookupFile(filedir: filedir, filename: filename, fileext: fileext)
                if(filePath != nil){
                    loadHTML(filePath: filePath!)
                }
            }
        }
        else if action == "special" && detailItem!.split(separator: ":")[1] == "feed" {
            showNotifications()
        }
    }
    
    // MARK: Load the HTML
    
    private func loadHTML(filePath: String) {
        //load html file
        do {
            let contents = try NSString(contentsOfFile: filePath, encoding: String.Encoding.utf8.rawValue)
            let correctedcontents = contents.replacingOccurrences(of: "<body>", with: "<body style=\"font: -apple-system-body\">")
            loadHTML(html: correctedcontents)
        }
        catch {
            print("Unexpected error: \(error).")
        }
    }
    
    private func loadHTML(html: String) {
        let version = Bundle.main.infoDictionary!["CFBundleShortVersionString"] as! String
        let htmlWithVersion = html.replacingOccurrences(of: "{{{version}}}", with: version)
        let htmlData = NSString(string: htmlWithVersion).data(using: String.Encoding.unicode.rawValue)
        
        
        let options = [NSAttributedString.DocumentReadingOptionKey.documentType: NSAttributedString.DocumentType.html]
        let attributedString = try! NSAttributedString(data: htmlData!, options: options, documentAttributes: nil)
        maintextview.attributedText = attributedString
        maintextview.textColor = .black
        if self.traitCollection.userInterfaceStyle == .dark {
            maintextview.textColor = .white
        }
        
        //scroll to top
        DispatchQueue.main.async {
            self.maintextview.scrollRangeToVisible(NSMakeRange(0, 0))
        }
    }
    
    // MARK: get announcement history
    
    private func showNotifications(){
        //turn on loading animation
        loadinganimation.startAnimating()
        //read sessionId
        let preferences = UserDefaults.standard
        let sessionId = preferences.string(forKey: "sessionId")
        if sessionId != nil {
            utils.getFeedHistory(sessionId: sessionId!, onComplete: { (responseString: String?) in
                if responseString != nil {
                    if responseString == "ERR_NO_SUCH_SESSION" {
                        DispatchQueue.main.async {
                            self.loadinganimation.stopAnimating()
                            self.loadLoginScreen()
                        }
                    }
                    else {
                        let html = self.utils.feedHistoryToHTML(feed: responseString! as String)
                        DispatchQueue.main.async {
                            self.loadinganimation.stopAnimating()
                            self.loadHTML(html: String(html))
                        }
                    }
                }
            })
        }
        else{
            loadLoginScreen()
        }
    }
    
    
    // MARK: viewDidLoad
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if detailItem != nil {
            configureView()
        }
        
    }
    // MARK: handle dark mode
    override func willTransition(to newCollection: UITraitCollection, with coordinator: UIViewControllerTransitionCoordinator) {
        if newCollection.userInterfaceStyle == .dark{
            maintextview.textColor = .white
        }
        else {
            maintextview.textColor = .black
        }
    }
    
    // MARK: load login screen
    func loadLoginScreen() {
        //load login screen
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let controller = storyboard.instantiateViewController(withIdentifier: "LoginController")
        self.present(controller, animated: true, completion: nil)
    }
}
