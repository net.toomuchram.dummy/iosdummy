//
//  AnnounceController.swift
//  dummy
//
//  Created by TooMuchRAM on 28/09/2019.
//  Copyright © 2019 TooMuchRAM. All rights reserved.
//

import UIKit

class AnnounceController: UIViewController {
    
    // MARK: properties
    @IBOutlet weak var announcementField: UITextField!
    @IBOutlet weak var loadinganimation: UIActivityIndicatorView!
    @IBOutlet weak var announcementInfoField: UILabel!
    
    // MARK: viewDidLoad
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        // (not needed)
    }
    

    // MARK: Send Announcement
    @IBAction func sendAnnouncement(_ sender: Any) {
        
        let announcement = announcementField.text
        announcementInfoField.text = " "
        announcementInfoField.textColor = .systemRed
        
        //check if the user is logged in
        let preferences = UserDefaults.standard
        let sessionId = preferences.string(forKey: "sessionId")
        
        //if announcement == nil {
            //announcementInfoField.text = "Geen tekst ingevoerd"
        //}
        //else
        if sessionId == nil {
            loadLoginScreen()
        }
        else {
            //start animation, because the internet related stuff can take a while
            loadinganimation.startAnimating()
            
            //build request
            let url = URL(string: "https://augustus.romereis-hl.nl/sendNotification")
            let params = "sessionId=" + sessionId! + "&message=" + announcement!;
            
            var request = URLRequest(url: url!)
            request.httpMethod = "POST"
            request.httpBody = params.data(using: .utf8)
            
            URLSession.shared.dataTask(with: request){ (data: Data?, response: URLResponse?, error: Error?) in
                if error != nil {
                    print("Error during request: \(String(describing: error))")
                    DispatchQueue.main.async{
                        self.announcementInfoField.text = "Onbekende fout"
                        self.loadinganimation.stopAnimating()
                    }
                    return
                }
                else{
                    let responseString = NSString(data: (data ?? nil)!, encoding: String.Encoding.utf8.rawValue) ?? nil
                    if responseString != nil {
                        DispatchQueue.main.async{
                            if responseString == "ERR_NO_MESSAGE" {
                                self.announcementInfoField.text = "Voer een bericht in"
                                self.loadinganimation.stopAnimating()
                            }
                            else if responseString == "ERR_MISSING_SESSIONID" {
                                self.announcementInfoField.text = "Onbekende fout"
                                self.loadinganimation.stopAnimating()
                            }
                            else if responseString == "ERR_MESSAGE_TOO_LONG" {
                                self.announcementInfoField.text = "Te lang bericht"
                                self.loadinganimation.stopAnimating()
                            }
                            else if responseString == "ERR_SESSIONID_TOO_LONG" || responseString == "ERR_SESSIONID_TOO_SHORT" {
                                self.announcementInfoField.text = "Onbekende fout"
                                self.loadinganimation.stopAnimating()
                            }
                            else if responseString == "ERR_NO_SUCH_SESSION" || responseString == "ERR_NO_SUCH_USER" {
                                self.loadinganimation.stopAnimating()
                                self.loadLoginScreen()
                            }
                            else if responseString == "ERR_TOO_MANY_USERS" {
                                self.announcementInfoField.text = "Onbekende fout"
                                self.loadinganimation.stopAnimating()
                            }
                            else if responseString == "ERR_UNAUTHORISED" {
                                self.announcementInfoField.text = "Onvoldoende rechten"
                                self.loadinganimation.stopAnimating()
                            }
                            else if responseString == "ERROR" {
                                self.announcementInfoField.text = "Onbekende fout"
                                self.loadinganimation.stopAnimating()
                            }
                            else if responseString == "SUCCESS" {
                                self.announcementInfoField.textColor = .systemGreen
                                self.announcementField.text = ""
                                self.announcementInfoField.text = "Verzonden"
                                DispatchQueue.main.asyncAfter(deadline: .now() + 2) { // change 2 to desired number of seconds
                                    self.announcementInfoField.text = " "
                                }
                                self.loadinganimation.stopAnimating()
                            }
                        }
                    }
                    else{
                        DispatchQueue.main.async{
                            self.announcementInfoField.text = "Onbekende fout"
                            self.loadinganimation.stopAnimating()
                        }
                    }
                }
            }.resume()
        }
    }
        
    // MARK: load login screen
    func loadLoginScreen() {
        //load login screen
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let controller = storyboard.instantiateViewController(withIdentifier: "LoginController")
        controller.modalPresentationStyle = .fullScreen //or .overFullScreen for transparency
        self.present(controller, animated: true, completion: nil)
    }

}
