//
//  TextViewControllerUtilities.swift
//  dummy
//
//  Created by TooMuchRAM on 28/09/2019.
//  Copyright © 2019 TooMuchRAM. All rights reserved.
//

import Foundation
struct TextViewControllerUtilities {
    
    // MARK: get feed history
    func getFeedHistory(sessionId: String, onComplete: @escaping (_ responseString: String?) -> Void ){
        let params = "sessionId=" + sessionId;
        let url = URL(string: "https://augustus.romereis-hl.nl/getAnnouncements?" + params)
        
        var request = URLRequest(url: url!)
        request.httpMethod = "GET"
        
        URLSession.shared.dataTask(with: request){ (data: Data?, response: URLResponse?, error: Error?) in
            if error != nil {
                print("Error during request: \(String(describing: error))")
                onComplete(nil)
            }
            else{
                let responseString = NSString(data: (data ?? nil)!, encoding: String.Encoding.utf8.rawValue) ?? nil
                onComplete(responseString as String?)
            }
        }.resume()
    }
    
    // MARK: feed history to HTML
    func feedHistoryToHTML(feed: String) -> String {
        let jsonData = feed.data(using: String.Encoding(rawValue: String.Encoding.utf8.rawValue))!
        let notifs = try! JSONDecoder().decode([Notification].self, from: jsonData)

        var html = """
        <!DOCTYPE html>
        <html>
            <body style="font: -apple-system-body; font-size: 12px;">
        """
        
        for notif in notifs {
            html += notif.notiftime + ": " + notif.message + "<br>"
        }
        
        html += """
            </body>
        </html>
        """
        return html
    }
}

//struct to hold the notifications
struct Notification: Decodable {
    let notiftime: String
    let message: String
}
