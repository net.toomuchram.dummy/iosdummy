//
//  MapDetailViewController.swift
//  dummy
//
//  Created by TooMuchRAM on 26/11/2019.
//  Copyright © 2019 TooMuchRAM. All rights reserved.
//

import UIKit
import SwiftPhotoGallery

class MapDetailViewController: UIViewController, SwiftPhotoGalleryDelegate, SwiftPhotoGalleryDataSource {
    // MARK: properties
    var itemTitle: String? {
        didSet {
            
        }
    }
    
    var contentFilename: String? {
        didSet {
            
        }
    }
    
    var imagePaths: [String]? = nil {
        didSet {
            if imagePaths != nil {
                images = fetchImages(fullfilenames: imagePaths!)
            }
        }
    }
    
    @IBOutlet weak var contentContainer: UITextView!
    
    private var images: [UIImage] = []
    private let appUtils = AppUtilities()
    
    // MARK: configureView
    func configureView(){
        addNavigationBarButtons()
        
        if itemTitle == nil || contentFilename == nil {
            return
        }
        loadHTML(fullfilename: contentFilename!)
        self.navigationItem.title = itemTitle

    }
    
    func addNavigationBarButtons(){
        let backItem = UIBarButtonItem()
        backItem.title = "Sluit"
        backItem.target = self
        backItem.action = #selector(closeController(sender:))
        self.navigationItem.leftBarButtonItem = backItem
        
        if images.count > 0 {
            let galleryItem = UIBarButtonItem()
            if images.count > 1 {
                galleryItem.title = "Foto's"
            }
            else {
                galleryItem.title = "Foto"
            }
            galleryItem.target = self
            galleryItem.action = #selector(showGallery(sender:))
            self.navigationItem.rightBarButtonItem = galleryItem
        }
    }
    
    // MARK: onClick listeners
    @objc func closeController(sender: UIBarButtonItem) {
        self.navigationController?.dismiss(animated: true, completion: nil)
    }
    
    @objc func showGallery(sender: UIBarButtonItem) {
        let gallery = createPhotoGallery()
        gallery.modalPresentationStyle = .fullScreen
        present(gallery, animated: true, completion: nil)
    }
    
    // MARK: Load the content
    // literally stolen from TextViewController
    private func loadHTML(fullfilename: String) {
        let fileext = NSString(string: String(fullfilename)).pathExtension
        let filename = NSString(string: NSString(string: String(fullfilename)).lastPathComponent).deletingPathExtension
        let filedir = NSString(string: String(fullfilename)).deletingLastPathComponent
        loadHTML(filePath: appUtils.lookupFile(filedir: filedir, filename: filename, fileext: fileext)!)
    }
    private func loadHTML(filePath: String) {
        //load html file
        do {
            let contents = try NSString(contentsOfFile: filePath, encoding: String.Encoding.utf8.rawValue)
            let correctedcontents = contents.replacingOccurrences(of: "<body>", with: "<body style=\"font: -apple-system-body\">")
            loadHTML(html: correctedcontents)
        }
        catch {
            print("Unexpected error: \(error).")
        }
    }
    
    private func loadHTML(html: String) {
        let htmlData = NSString(string: html).data(using: String.Encoding.unicode.rawValue)
        let options = [NSAttributedString.DocumentReadingOptionKey.documentType: NSAttributedString.DocumentType.html]
        let attributedString = try! NSAttributedString(data: htmlData!, options: options, documentAttributes: nil)
        contentContainer.attributedText = attributedString
        contentContainer.textColor = .black
        if self.traitCollection.userInterfaceStyle == .dark {
            contentContainer.textColor = .white
        }
        
        //scroll to top
        DispatchQueue.main.async {
            self.contentContainer.scrollRangeToVisible(NSMakeRange(0, 0))
        }
    }
    
    // MARK: image-related stuff
    @objc func createPhotoGallery() -> SwiftPhotoGallery{
        let gallery = SwiftPhotoGallery(delegate: self, dataSource: self)
        gallery.backgroundColor = UIColor.black
        gallery.pageIndicatorTintColor = UIColor.gray.withAlphaComponent(0.5)
        gallery.currentPageIndicatorTintColor = UIColor.white
        if images.count > 1 {
            gallery.hidePageControl = false
        }
        else {
            gallery.hidePageControl = true
        }
        return gallery
    }
    
    func fetchImages(fullfilenames: [String]) -> [UIImage] {
        var images: [UIImage] = []
        for fullfilename in fullfilenames {
            
            let fileext = NSString(string: String(fullfilename)).pathExtension
            let filename = NSString(string: NSString(string: String(fullfilename)).lastPathComponent).deletingPathExtension
            let filedir = NSString(string: String(fullfilename)).deletingLastPathComponent
            
            let filePath = appUtils.lookupFile(filedir: filedir, filename: filename, fileext: fileext)
            guard let img = UIImage(contentsOfFile: filePath!) else {
                break
            }
            images.append(img)
        }
        return images
    }
    
    // MARK: SwiftPhotoGallery delegates
    func numberOfImagesInGallery(gallery: SwiftPhotoGallery) -> Int {
        return images.count
    }

    func imageInGallery(gallery: SwiftPhotoGallery, forIndex: Int) -> UIImage? {
        return images[forIndex]
    }
    
    func galleryDidTapToClose(gallery: SwiftPhotoGallery) {
        // do something cool like:
        dismiss(animated: true, completion: nil)
    }
    
    // MARK: overrides
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        configureView()
    }
    override func willTransition(to newCollection: UITraitCollection, with coordinator: UIViewControllerTransitionCoordinator) {
        if newCollection.userInterfaceStyle == .dark {
            contentContainer.textColor = .white
        }
        else {
            contentContainer.textColor = .black
        }
    }
}

