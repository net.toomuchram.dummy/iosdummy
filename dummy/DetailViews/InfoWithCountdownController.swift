//
//  InfoWithCountdownController.swift
//  dummy
//
//  Created by TooMuchRAM on 23/01/2020.
//  Copyright © 2020 TooMuchRAM. All rights reserved.
//

import UIKit
import GrowingTextView

class InfoWithCountdownController: UIViewController {
    
    var releaseDate: NSDate?
    var countdownTimer = Timer()
    @IBOutlet weak var countdownLabel: UILabel!
    @IBOutlet weak var yearProgress: UIProgressView!
    
    private let appUtils = AppUtilities()
    
    func loadInfo() {
        let html = appUtils.openHTMLfromDisk(name: String("info.html")) ?? ""
        let correctedhtml = html
            .replacingOccurrences(of: "<body>", with: "<body style=\"font: -apple-system-body\">")
            .replacingOccurrences(of: "</body>", with: "<br><h1>Countdown</h1></body>") /* Ultra dirty workaround*/
        loadHTML(html: correctedhtml)
    }
    
    private func loadHTML(html: String) {
        let htmlData = NSString(string: html).data(using: String.Encoding.unicode.rawValue)
        let options = [NSAttributedString.DocumentReadingOptionKey.documentType: NSAttributedString.DocumentType.html]
        let attributedString = try! NSAttributedString(data: htmlData!, options: options, documentAttributes: nil)
        maintextview.attributedText = attributedString
        maintextview.textColor = .black
        if self.traitCollection.userInterfaceStyle == .dark {
            maintextview.textColor = .white
        }
        
        //scroll to top
        DispatchQueue.main.async {
            self.maintextview.scrollRangeToVisible(NSMakeRange(0, 0))
        }
    }
    
    @IBOutlet weak var maintextview: GrowingTextView!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        loadInfo()
        correctFont()
        startTimer()
    }
    
    // MARK: handle dark mode
    override func willTransition(to newCollection: UITraitCollection, with coordinator: UIViewControllerTransitionCoordinator) {
        if newCollection.userInterfaceStyle == .dark{
            maintextview.textColor = .white
        }
        else {
            maintextview.textColor = .black
        }
    }
    
    // MARK: -COUNTDOWN RELATED FUNCTIONS
    
    func correctFont() {
        
        // https://stackoverflow.com/questions/38560422/how-to-make-characters-equal-width-in-uilabel
        let bodyFontDescriptor = UIFontDescriptor.preferredFontDescriptor(withTextStyle: UIFont.TextStyle.body)
        let bodyMonospacedNumbersFontDescriptor = bodyFontDescriptor.addingAttributes(
            [
                UIFontDescriptor.AttributeName.featureSettings: [
                    [
                        UIFontDescriptor.FeatureKey.featureIdentifier: kNumberSpacingType,
                        UIFontDescriptor.FeatureKey.typeIdentifier: kMonospacedNumbersSelector
                    ]
                ]
            ])

        let bodyMonospacedNumbersFont = UIFont(descriptor: bodyMonospacedNumbersFontDescriptor, size: 30.0)
        countdownLabel.font = bodyMonospacedNumbersFont
    }
    
    func startTimer() {
        releaseDate = appUtils.getRomereisDate()
        updateTime()
        countdownTimer = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(updateTime), userInfo: nil, repeats: true)
    }

    @objc func updateTime() {

        let currentDate = Date()
        let calendar = Calendar.current
        
        let countdown: NSMutableAttributedString!
        if ((releaseDate! as Date) < currentDate) {
            countdown = NSMutableAttributedString(string: "00d 00u 00m 00s")
            yearProgress.progress = 1
        }
        else {
            let diffDateComponents = calendar.dateComponents([.day, .hour, .minute, .second], from: currentDate, to: releaseDate! as Date)
            
            
            let days: String!
            if (diffDateComponents.day ?? 0) < 10 {
                days = "0" + String((diffDateComponents.day ?? 0)!)
            }
            else {
                days = String((diffDateComponents.day ?? 0)!)
            }
            
            let hours: String!
            if (diffDateComponents.hour ?? 0) < 10 {
                hours = "0" + String((diffDateComponents.hour ?? 0)!)
            }
            else {
                hours = String((diffDateComponents.hour ?? 0)!)
            }
            
            let minutes: String!
            if (diffDateComponents.minute ?? 0) < 10 {
                minutes = "0" + String((diffDateComponents.minute ?? 0)!)
            }
            else {
                minutes = String((diffDateComponents.minute ?? 0)!)
            }
            
            let seconds: String!
            if (diffDateComponents.second ?? 0) < 10 {
                seconds = "0" + String((diffDateComponents.second ?? 0)!)
            }
            else {
                seconds = String((diffDateComponents.second ?? 0)!)
            }

            countdown = NSMutableAttributedString(string: "\(String(describing: days!))d \(String(describing: hours!))u \(String(describing: minutes!))m \(String(describing: seconds!))s")
            
            let diff = (releaseDate!.timeIntervalSince1970 * 1000).rounded() - (Date().timeIntervalSince1970 * 1000).rounded()
            
            if (diff) >= 0 {
                yearProgress.progress = 1 - Float(diff)/Float(365 * 24 * 60 * 60 * 1000)
            }
        }

    
        countdown.setColourForText("d", with: .gray)
        countdown.setColourForText("u", with: .gray)
        countdown.setColourForText("m", with: .gray)
        countdown.setColourForText("s", with: .gray)


        countdownLabel.attributedText = countdown
        
    }

}
