//
//  WebViewController.swift
//  dummy
//
//  Created by TooMuchRAM on 12/10/2019.
//  Copyright © 2019 TooMuchRAM. All rights reserved.
//

import UIKit
import WebKit
import SwiftEventBus

class WebViewController: UIViewController, WKNavigationDelegate {
    
    
    // MARK: properties
    var url: String? {
        didSet {
            //configureView()
        }
    }
    private var chatController: ChatController? = nil
    @IBOutlet weak var mainwebview: WKWebView!

    let accountUtils = AccountUtilities()

    // MARK: configureView
    func configureView(){
        if url == nil {
            return
        }
        
        
        mainwebview.navigationDelegate = self
        let type = url!.split(separator: ":")[0]
        if type == "remote" {
            let link = URL(string: "https://" + String(url!.split(separator: ":")[1]))!
            loadRemoteContent(url: link)
        }
        else if type == "local" {
            
            let fullfilename = url!.split(separator: ":")[1]

            if let dotRange = fullfilename.range(of: "?") {
                var filename = fullfilename
                filename.removeSubrange(dotRange.lowerBound..<fullfilename.endIndex)
                loadLocalContent(fullfilename: String(filename))
            } else {
                loadLocalContent(fullfilename: String(fullfilename))
            }
            if fullfilename == "assets/chat.html" {
                self.chatController = ChatController(controller: self, adminchatEnabled: false)
            } else if fullfilename == "assets/chat.html?admin" {
                self.chatController = ChatController(controller: self, adminchatEnabled: true)
            }
        }
    }
    
    // MARK: load content
    func loadRemoteContent(url: URL) {
        let request = URLRequest(url: url)
        mainwebview.load(request)
    }
    
    func loadLocalContent(fullfilename: String){
        let fileext = NSString(string: String(fullfilename)).pathExtension
        let filename = NSString(string: NSString(string: String(fullfilename)).lastPathComponent).deletingPathExtension
        let filedir = NSString(string: String(fullfilename)).deletingLastPathComponent
        let url = Bundle.main.url(forResource: filename, withExtension: fileext, subdirectory: filedir)
        
        if url != nil {
            mainwebview.loadFileURL(url!, allowingReadAccessTo: url!)
            let request = URLRequest(url: url!)
            mainwebview.load(request)
        }
    }

    // MARK: overrides
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        configureView()
    }
    // MARK: load login screen
    func loadLoginScreen() {
        //load login screen
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let controller = storyboard.instantiateViewController(withIdentifier: "LoginController")
        controller.modalPresentationStyle = .fullScreen //or .overFullScreen for transparency
        self.present(controller, animated: true, completion: nil)
    }
    
    func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
        if chatController != nil && webView.url?.lastPathComponent == "chat.html" {
            //If the ChatController is loaded, initialise the chat functionality
            chatController?.initChat()
        }
    }
    
}
