//
//  CountdownController.swift
//  dummy
//
//  Created by TooMuchRAM on 03/12/2019.
//  Copyright © 2019 TooMuchRAM. All rights reserved.
//

import UIKit

class CountdownController: UIViewController {
    
    var releaseDate: NSDate?
    var countdownTimer = Timer()
    let appUtils = AppUtilities()

    @IBOutlet weak var countdownLabel: UILabel!
    @IBOutlet weak var yearProgress: UIProgressView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        // https://stackoverflow.com/questions/38560422/how-to-make-characters-equal-width-in-uilabel
        let bodyFontDescriptor = UIFontDescriptor.preferredFontDescriptor(withTextStyle: UIFont.TextStyle.body)
        let bodyMonospacedNumbersFontDescriptor = bodyFontDescriptor.addingAttributes(
            [
                UIFontDescriptor.AttributeName.featureSettings: [
                    [
                        UIFontDescriptor.FeatureKey.featureIdentifier: kNumberSpacingType,
                        UIFontDescriptor.FeatureKey.typeIdentifier: kMonospacedNumbersSelector
                    ]
                ]
            ])

        let bodyMonospacedNumbersFont = UIFont(descriptor: bodyMonospacedNumbersFontDescriptor, size: 30.0)
        countdownLabel.font = bodyMonospacedNumbersFont
        
        startTimer()
    }

    func startTimer() {
        releaseDate = appUtils.getRomereisDate()
        updateTime()
        countdownTimer = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(updateTime), userInfo: nil, repeats: true)
    }

    @objc func updateTime() {

        let currentDate = Date()
        let calendar = Calendar.current
        
        let countdown: NSMutableAttributedString!
        if ((releaseDate! as Date) < currentDate) {
            countdown = NSMutableAttributedString(string: "00d 00u 00m 00s")
            yearProgress.progress = 1
        }
        else {
            let diffDateComponents = calendar.dateComponents([.day, .hour, .minute, .second], from: currentDate, to: releaseDate! as Date)
            
            
            let days: String!
            if (diffDateComponents.day ?? 0) < 10 {
                days = "0" + String((diffDateComponents.day ?? 0)!)
            }
            else {
                days = String((diffDateComponents.day ?? 0)!)
            }
            
            let hours: String!
            if (diffDateComponents.hour ?? 0) < 10 {
                hours = "0" + String((diffDateComponents.hour ?? 0)!)
            }
            else {
                hours = String((diffDateComponents.hour ?? 0)!)
            }
            
            let minutes: String!
            if (diffDateComponents.minute ?? 0) < 10 {
                minutes = "0" + String((diffDateComponents.minute ?? 0)!)
            }
            else {
                minutes = String((diffDateComponents.minute ?? 0)!)
            }
            
            let seconds: String!
            if (diffDateComponents.second ?? 0) < 10 {
                seconds = "0" + String((diffDateComponents.second ?? 0)!)
            }
            else {
                seconds = String((diffDateComponents.second ?? 0)!)
            }

            countdown = NSMutableAttributedString(string: "\(String(describing: days!))d \(String(describing: hours!))u \(String(describing: minutes!))m \(String(describing: seconds!))s")
            
            
            if (diffDateComponents.day ?? 0) >= 0 {
                yearProgress.progress = 1 - Float((diffDateComponents.day ?? 0))/Float(365)
            }
        }

    
        countdown.setColourForText("d", with: .gray)
        countdown.setColourForText("u", with: .gray)
        countdown.setColourForText("m", with: .gray)
        countdown.setColourForText("s", with: .gray)


        countdownLabel.attributedText = countdown
        
    }

}
extension NSMutableAttributedString{
    func setColourForText(_ textToFind: String, with color: UIColor) {
        let range = self.mutableString.range(of: textToFind, options: .caseInsensitive)
        if range.location != NSNotFound {
            addAttribute(NSAttributedString.Key.foregroundColor, value: color, range: range)
        }
    }
}
