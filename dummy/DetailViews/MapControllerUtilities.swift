//
//  MapControllerUtilities.swift
//  dummy
//
//  Created by TooMuchRAM on 29/09/2019.
//  Copyright © 2019 TooMuchRAM. All rights reserved.
//

import Foundation
import Mapbox
import SwiftEventBus

struct MapControllerUtilities {
    func createPOImarkers() -> [BuildingMarker] {
        let forumromanum = BuildingMarker()
        forumromanum.coordinate = CLLocationCoordinate2D(latitude: 41.892470, longitude: 12.485329)
        forumromanum.title = "Forum Romanum"
        forumromanum.contentFile = "geotracker/gebouwen/forumromanum.html"
        forumromanum.images = ["geotracker/gebouwen/forumromanum0.jpg", "geotracker/gebouwen/forumromanum1.jpg"]
        
        let cloacamaxima = BuildingMarker()
        cloacamaxima.coordinate = CLLocationCoordinate2D(latitude: 41.888902, longitude: 12.480262)
        cloacamaxima.title = "Cloaca Maxima"
        cloacamaxima.contentFile = "geotracker/gebouwen/cloacamaxima.html"
        cloacamaxima.images = ["geotracker/gebouwen/cloacamaxima.jpg", "geotracker/gebouwen/cloaca-maxima-binnenkant.jpg"]
        
        let forumboarium = BuildingMarker()
        forumboarium.coordinate = CLLocationCoordinate2D(latitude: 41.888748, longitude: 12.480746)
        forumboarium.title = "Forum Boarium"
        forumboarium.contentFile = "geotracker/gebouwen/forumboarium.html"
        forumboarium.images = ["geotracker/gebouwen/forumboarium0.jpg", "geotracker/gebouwen/forumboarium1.jpg"]
        
        let forumjulium = BuildingMarker()
        forumjulium.coordinate = CLLocationCoordinate2D(latitude: 41.893930, longitude: 12.485039)
        forumjulium.title = "Forum Julium"
        forumjulium.contentFile = "geotracker/gebouwen/forumjulium.html"
        forumjulium.images = ["geotracker/gebouwen/forumjulium.jpg"]
        
        let largoDiTorreArgentina = BuildingMarker()
        largoDiTorreArgentina.coordinate = CLLocationCoordinate2D(latitude: 41.895411, longitude: 12.476860)
        largoDiTorreArgentina.title = "Largo Di Torre Argentina"
        largoDiTorreArgentina.contentFile = "geotracker/gebouwen/largoditorreargentina.html"
        largoDiTorreArgentina.images = ["geotracker/gebouwen/largoditorreargentina0.jpg", "geotracker/gebouwen/largoditorreargentina1.jpg"]
        
        let tibereiland = BuildingMarker()
        tibereiland.coordinate = CLLocationCoordinate2D(latitude: 41.890466, longitude: 12.477742)
        tibereiland.title = "Tibereiland"
        tibereiland.contentFile = "geotracker/gebouwen/tibereiland.html"
        tibereiland.images = ["geotracker/gebouwen/tibereiland.jpg"]
        
        let theatreOfMarcellus = BuildingMarker()
        theatreOfMarcellus.coordinate = CLLocationCoordinate2D(latitude: 41.89194, longitude: 12.47975)
        theatreOfMarcellus.title = "Theater van Marcellus"
        theatreOfMarcellus.contentFile = "geotracker/gebouwen/theatervanmarcellus.html"
        theatreOfMarcellus.images = ["geotracker/gebouwen/theatervanmarcellus0.jpg", "geotracker/gebouwen/theatervanmarcellus1.jpg"]
        
        let forumAugustum = BuildingMarker()
        forumAugustum.coordinate = CLLocationCoordinate2D(latitude: 41.89429, longitude: 12.48681)
        forumAugustum.title = "Forum Augustum"
        forumAugustum.contentFile = "geotracker/gebouwen/forumaugustum.html"
        forumAugustum.images = ["geotracker/gebouwen/forumaugustum.jpg"]
        
        let araPacis = BuildingMarker()
        araPacis.coordinate = CLLocationCoordinate2D(latitude: 41.90613, longitude: 12.47546)
        araPacis.title = "Ara Pacis"
        araPacis.contentFile = "geotracker/gebouwen/arapacis.html"
        araPacis.images = ["geotracker/gebouwen/arapacis-voorkant.jpg", "geotracker/gebouwen/arapacis-achterkant.jpg"]
        
        let mausoleumAugustum = BuildingMarker()
        mausoleumAugustum.coordinate = CLLocationCoordinate2D(latitude: 41.90602, longitude: 12.47642)
        mausoleumAugustum.title = "Mausoleum van Augustus"
        mausoleumAugustum.contentFile = "geotracker/gebouwen/mausoleumaugustum.html"
        mausoleumAugustum.images = ["geotracker/gebouwen/mausoleumvanaugustus.jpg"]
        
        let templeOfAgrippa = BuildingMarker()
        templeOfAgrippa.coordinate = CLLocationCoordinate2D(latitude: 41.8986, longitude: 12.47683)
        templeOfAgrippa.title = "Tempel van Agrippa"
        templeOfAgrippa.contentFile = "geotracker/gebouwen/tempelvanagrippa.html"
        templeOfAgrippa.images = ["geotracker/gebouwen/pantheon0.jpg", "geotracker/gebouwen/pantheon1.jpg"]
        
        let domusAurea = BuildingMarker()
        domusAurea.coordinate = CLLocationCoordinate2D(latitude: 41.89140, longitude: 12.49469)
        domusAurea.title = "Domus Aurea"
        domusAurea.contentFile = "geotracker/gebouwen/domusaurea.html"
        domusAurea.images = ["geotracker/gebouwen/domusaurea.jpg"]
        
        let colosseum = BuildingMarker()
        colosseum.coordinate = CLLocationCoordinate2D(latitude: 41.89025, longitude: 12.49230)
        colosseum.title = "Colosseum"
        colosseum.contentFile = "geotracker/gebouwen/colosseum.html"
        colosseum.images = ["geotracker/gebouwen/colosseum-binnenkant.png", "geotracker/gebouwen/colosseum-buiten.jpg"]
        
        let forumTrajanum = BuildingMarker()
        forumTrajanum.coordinate = CLLocationCoordinate2D(latitude: 41.89521, longitude: 12.48560)
        forumTrajanum.title = "Forum Trajanum"
        forumTrajanum.contentFile = "geotracker/gebouwen/forumtrajanum.html"
        forumTrajanum.images = ["geotracker/gebouwen/forumtrajanum0.jpg", "geotracker/gebouwen/forumtrajanum1.jpg", "geotracker/gebouwen/zuiltrajanus.png"]
        
        let bathsOfDiocletian = BuildingMarker()
        bathsOfDiocletian.coordinate = CLLocationCoordinate2D(latitude: 41.90323, longitude: 12.49701)
        bathsOfDiocletian.title = "Thermen van Diocletianus"
        bathsOfDiocletian.contentFile = "geotracker/gebouwen/thermendiocletianus.html"
        bathsOfDiocletian.images = ["geotracker/gebouwen/thermendiocletianus0.jpg", "geotracker/gebouwen/thermendiocletianus1.jpg"]
        
        let muurHadrianus = BuildingMarker()
        muurHadrianus.coordinate = CLLocationCoordinate2D(latitude: 54.99648, longitude: -1.78658)
        muurHadrianus.title = "Muur van Hadrianus"
        muurHadrianus.contentFile = "geotracker/gebouwen/hadriaansemuur.html"
        muurHadrianus.images = ["geotracker/gebouwen/hadrianusmuur.jpg"]
        
        let mausoleumHadrianum = BuildingMarker()
        mausoleumHadrianum.coordinate = CLLocationCoordinate2D(latitude: 41.90304, longitude: 12.46634)
        mausoleumHadrianum.title = "Mausoleum van Hadrianus"
        mausoleumHadrianum.contentFile = "geotracker/gebouwen/mausoleumhadrianus.html"
        mausoleumHadrianum.images = ["geotracker/gebouwen/mausoleumvanhadrianus0.jpg", "geotracker/gebouwen/mausoleumvanhadrianus1.jpg"]
        
        let tempelVenusRoma = BuildingMarker()
        tempelVenusRoma.coordinate = CLLocationCoordinate2D(latitude: 41.89082,longitude: 12.48999)
        tempelVenusRoma.title = "Tempel van Venus en Roma"
        tempelVenusRoma.contentFile = "geotracker/gebouwen/tempelvenusroma.html"
        tempelVenusRoma.images = ["geotracker/gebouwen/tempelvenusroma0.jpg", "geotracker/gebouwen/tempelvenusroma1.jpg"]
        
        
        
        return [forumromanum, cloacamaxima, forumboarium, forumjulium, largoDiTorreArgentina, tibereiland, theatreOfMarcellus, forumAugustum, araPacis, mausoleumAugustum, templeOfAgrippa, domusAurea, colosseum, forumTrajanum, bathsOfDiocletian, muurHadrianus, mausoleumHadrianum, tempelVenusRoma]
    }
    
    
    func requestCachedLocations() {
        let preferences = UserDefaults.standard
        let sessionId = preferences.string(forKey: "sessionId")
        if sessionId != nil {
            SwiftEventBus.post("sendWSmessage", sender: "{\"channel\":\"getCachedLocations\", \"sessionId\":\"\(String(describing: sessionId!))\"}")
        }
    }
}
