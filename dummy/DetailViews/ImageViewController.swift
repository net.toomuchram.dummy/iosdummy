import UIKit
import ImageScrollView

//The only point left for this file is to provide an image view controller for the chat images.

@objc class ImageViewController: UIViewController {
    
    // MARK: properties
    
    @IBOutlet weak var imageScrollView: ImageScrollView!
    
    var image: UIImage? {
        didSet {
            
        }
    }
    
    // MARK: configureView
    func configureView() {
        
        // Important: This setup method should be called once, usually in your viewDidLoad() method
        if(image != nil) {
            imageScrollView.setup()
            imageScrollView.display(image: image!)
            imageScrollView.backgroundColor = image?.getColors()?.background
        }
        self.navigationController?.hidesBarsOnTap = true;
    }
    
    // MARK: overrides
    override func viewDidLoad(){
        configureView()
    }
    
}
