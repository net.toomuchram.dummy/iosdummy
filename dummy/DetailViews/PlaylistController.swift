//
//  PlaylistController.swift
//  dummy
//
//  Created by TooMuchRAM on 23/02/2020.
//  Copyright © 2020 TooMuchRAM. All rights reserved.
//

import UIKit
import AVFoundation
import AVKit

class PlaylistController: UITableViewController {
    
    private let tracks = [("Fountain of Valle Giulia at Dawn", "audio/valle-giulia-dawn.mp3"), ("Triton Fountain at Morn", "audio/triton-morn.mp3"), ("Fountain of Trevi at Mid-day", "audio/trevi-mid-day.mp3"), ("Villa Medici Fountain at Sunset", "audio/villa-medici-sunset.mp3"), ("Santana - Maria Maria", "audio/mariamaria.mp3")]
    private let appUtils = AppUtilities()
    
    private var player: AVPlayer!

    override func viewDidLoad() {
        super.viewDidLoad()
        
        do {
            try AVAudioSession.sharedInstance().setActive(true)
        } catch {
            print("Error during activation of audio session: \(error)")
        }

        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return tracks.count
    }

    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "reuseIdentifier", for: indexPath)

        // Configure the cell...
        cell.textLabel!.text = tracks[indexPath.row].0

        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let fullfilename = tracks[indexPath.row].1
        let fileext = NSString(string: String(fullfilename)).pathExtension
        let filename = NSString(string: NSString(string: String(fullfilename)).lastPathComponent).deletingPathExtension
        let filedir = NSString(string: String(fullfilename)).deletingLastPathComponent
        
        guard
            let audioURL = Bundle.main.url(forResource: String(filename), withExtension: String(fileext), subdirectory: "assets/" + String(filedir))
            else {
            print("File reading error")
            return
        }
        
        let asset = AVAsset(url: audioURL)
        let playerItem = AVPlayerItem(asset: asset)
        
        let titleItem = AVMutableMetadataItem()
        titleItem.identifier = AVMetadataIdentifier.commonIdentifierTitle
        titleItem.value = tracks[indexPath.row].0 as NSCopying & NSObjectProtocol
        
        playerItem.externalMetadata = [titleItem]
        
        player = AVPlayer(playerItem: playerItem)
        let playerViewController = AVPlayerViewController()
        playerViewController.player = player
        
        self.present(playerViewController, animated: true) {
            self.player.play()
        }
        
    }
    

    override func viewDidDisappear(_ animated: Bool) {
        do {
            try AVAudioSession.sharedInstance().setActive(false)
        } catch {
            print("Error during deactivation of audio session: \(error)")
        }
    }
    
    /*
    // Override to support conditional editing of the table view.
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    */

    /*
    // Override to support editing the table view.
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            // Delete the row from the data source
            tableView.deleteRows(at: [indexPath], with: .fade)
        } else if editingStyle == .insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    */

    /*
    // Override to support rearranging the table view.
    override func tableView(_ tableView: UITableView, moveRowAt fromIndexPath: IndexPath, to: IndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the item to be re-orderable.
        return true
    }
    */

}
