//
//  ChatController.swift
//  dummy
//
//  Created by TooMuchRAM on 09/11/2019.
//  Copyright © 2019 TooMuchRAM. All rights reserved.
//

import Foundation
import WebKit
import SwiftEventBus

class ChatController: NSObject, WKUIDelegate {
    
    
    // MARK: - properties and init
    
    var mainwebview: WKWebView
    var webviewcontroller: WebViewController
    var accountUtils: AccountUtilities
    var adminchat: Bool
    var loadedMessages: [Int: RomereisChatMessage] = [:]
    
    init(controller: WebViewController, adminchatEnabled: Bool = false) {
        accountUtils = AccountUtilities()
        mainwebview = controller.mainwebview
        webviewcontroller = controller
        adminchat = adminchatEnabled
        super.init()
    }
    
    
    
    // MARK: initialise chat
    func initChat() {
        
        print("Initialising chat...")
        let sessionId = accountUtils.sessionId
        if sessionId != nil {
            getAndLoadChatHistory(sessionId: sessionId!, days: 1, to:0)
            mainwebview.uiDelegate = self
        }
    }
    
    func getAndLoadChatHistory(sessionId: String, days: Int, to: Int) {
        getChatHistory(days: days, to: to, callback: { chathistory in
            if chathistory != nil {
                if(chathistory?.count == 0) {
                    self.getAndLoadChatHistory(sessionId: sessionId, days: (days + (days-to)), to: (to + (days-to)))
                    return
                }
                
                for (i, msg) in chathistory!.enumerated() {
                    self.loadedMessages[msg.id] = msg
                    if i > 1 {
                        let previousMessage = chathistory![i - 1]
                        if previousMessage.user == msg.user {
                            self.addMessage(messageObject: msg, withHeader: false, atBeginning: false)
                        } else {
                            self.addMessage(messageObject: msg, withHeader: true, atBeginning: false)
                        }
                    } else {
                        self.addMessage(messageObject: msg, atBeginning: false)
                    }
                }
                self.mainwebview.evaluateJavaScript("window.loadedDays = " + String(days))
            }
             
            self.mainwebview.evaluateJavaScript("addLoadMoreMessagesButton()")
            self.registerChatListener()
            }
        )
    }
    
    // MARK: get chat history
    func getChatHistory(days: Int = 1, to: Int = 0, callback: @escaping (_ chathistory: [RomereisChatMessage]?) -> Void) {
        let sessionId = accountUtils.sessionId

        if sessionId != nil {
            let url: URL!
            if adminchat {
                url = URL(string: "https://augustus.romereis-hl.nl/getMessages?sessionId=\(sessionId!)&days=\(days)&to=\(to)&type=admin")!
            } else {
                url = URL(string: "https://augustus.romereis-hl.nl/getMessages?sessionId=\(sessionId!)&days=\(days)&to=\(to)")!
            }

            URLSession.shared.dataTask(with: url) { (data, response, error) in
                if data != nil {
                    let responseString = String(data: data!, encoding: .utf8)!
                    if responseString != "ERR_MISSING_SESSIONID" || responseString != "ERR_NO_SUCH_SESSION" || responseString != "ERROR" {
                        DispatchQueue.main.async {
                            do {
                                let jsonData = (responseString).data(using: String.Encoding(rawValue: String.Encoding.utf8.rawValue))
                                let msgs = try! JSONDecoder().decode([RomereisChatMessage].self, from: jsonData!)
                                callback(msgs)
                            } catch {
                                print(error)
                            }
                        }
                    }
                    else {
                        callback(nil)
                    }
                }
            }.resume()
        }
    }
    
    // MARK: register listener
    func registerChatListener() {
        let target = SwiftEventBus.self
        
        let name: String!
        if adminchat {
            name = "Adminchat"
        } else {
            name = "Chat"
        }
        
        SwiftEventBus.onMainThread(target, name: name) { result in
            let messageObject = result?.object as! RomereisChatMessage
            
            if self.loadedMessages[messageObject.id] != nil {
                // the messages has already been loaded
                // stop execution to prevent duplicates from showing up
                return
            }
            
            self.loadedMessages[messageObject.id] = messageObject
            
            let previousMessage = self.loadedMessages[messageObject.id - 1]
            if previousMessage != nil, previousMessage!.user == messageObject.user {
                self.addMessage(messageObject: messageObject, withHeader: false, atBeginning: false)
            } else {
                self.addMessage(messageObject: messageObject, atBeginning: false)
            }
        }
    }
    
    // MARK: add message to UI
    func addMessage(messageObject: RomereisChatMessage, withHeader: Bool = true, atBeginning: Bool = false) {
        let javascript = """
            addMessage(
                {
            "id": \(messageObject.id),
            "message": "\(messageObject.message)",
            "user": "\(messageObject.user)",
            "imgpath": "\(messageObject.imgpath)",
            "timestamp": "\(messageObject.timestamp)"
            }, \(withHeader), \(atBeginning)
            )
        """
        self.mainwebview.evaluateJavaScript(javascript)
    }
    
    // MARK: send message
    func sendMessage(message: String, callback: @escaping () -> Void) {
        let sessionId = accountUtils.sessionId

        if sessionId != nil {
            let url = URL(string: "https://augustus.romereis-hl.nl/sendMessage")
            var params = "sessionId=" + sessionId! + "&message=" + message;
            if adminchat {
                params += "&type=admin"
            }
            
            var request = URLRequest(url: url!)
            request.httpMethod = "POST"
            request.httpBody = params.data(using: .utf8)
            
            URLSession.shared.dataTask(with: request){ (data: Data?, response: URLResponse?, error: Error?) in
                callback()
            }.resume()
        }
    }
    
    // MARK: interface with JS
    func webView(_ webView: WKWebView, runJavaScriptTextInputPanelWithPrompt prompt: String, defaultText: String?, initiatedByFrame frame: WKFrameInfo, completionHandler: @escaping (String?) -> Void) {
        do {
            let jsonData = (prompt).data(using: String.Encoding(rawValue: String.Encoding.utf8.rawValue))
            let message = try JSONDecoder().decode(JSWebKitMessage.self, from: jsonData!)
            if message.action == "send" && message.message != nil{
                sendMessage(message: message.message!, callback: {
                    OperationQueue.main.addOperation({
                        self.mainwebview.evaluateJavaScript("document.getElementById('messagetext').value = ''")
                        self.mainwebview.evaluateJavaScript("document.getElementById('messagebutton').disabled = false")
                    })
                })
            }
            else if message.action == "loadMoreMessages" && message.loadedDays != nil && message.increaseBy != nil  {
                loadMoreMessages(days: message.loadedDays! + message.increaseBy!, to: message.loadedDays!)
            }
            else if message.action == "print" && message.debugMessage != nil {
                print(message.debugMessage!)
            }
        } catch {
            print("An error occured during the JSON parsing")
        }
        completionHandler(defaultText)
    }

    func loadMoreMessages(days: Int, to: Int) {
        getChatHistory(
                days: days,
                to: to,
                callback: { chathistory in
                    if chathistory != nil {
                        let reversedChatHistory = chathistory!.reversed() as Array
                        for (i, msg) in reversedChatHistory.enumerated() {
                            self.loadedMessages[msg.id] = msg
                            if i < reversedChatHistory.count - 1 {
                                let previousMessage = reversedChatHistory[i + 1]
                                if previousMessage.user == msg.user {
                                    self.addMessage(messageObject: msg, withHeader: false, atBeginning: true)
                                } else {
                                    self.addMessage(messageObject: msg, withHeader: true, atBeginning: true)
                                }
                            } else {
                                self.addMessage(messageObject: msg, atBeginning: true)
                            }
                        }
                        self.mainwebview.evaluateJavaScript("loadedDays += \(days-to)")
                        if chathistory!.count == 0 {
                            self.loadMoreMessages(days: days + (days-to), to: to + (days-to))
                        }
                        else {
                            self.mainwebview.evaluateJavaScript("addLoadMoreMessagesButton()")
                        }
                        
                    }
                }
        )
    }
    
}

struct JSWebKitMessage: Codable {
    let action: String
    // If the user wants to send a message
    let message: String?
    // If the user wants to load more messages
    let loadedDays: Int?
    let increaseBy: Int?
    // If the JS code wants to print something to console
    let debugMessage: String?
}
