//
//  MapController.swift
//  dummy
//
//  Created by TooMuchRAM on 29/09/2019.
//  Copyright © 2019 TooMuchRAM. All rights reserved.
//

import UIKit
import Mapbox
import SwiftEventBus
import LocalizedTimeAgo

class MapController: UIViewController, MGLMapViewDelegate {
    
    // MARK: properties
    @IBOutlet weak var mapView: MGLMapView!
    
    var userMarkers: [String:UserMarker] = [:]
    let utils = MapControllerUtilities()
    
    // MARK: configureView
    func configureView(){
        //load the map
        var url: URL!
        if self.traitCollection.userInterfaceStyle == .dark {
            url = URL(string: "mapbox://styles/mapbox/dark-v10")
        }
        else {
            url = URL(string: "mapbox://styles/mapbox/streets-v11")
        }
        mapView.styleURL = url
        //set options
        mapView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        mapView.delegate = self as MGLMapViewDelegate
        mapView.setCenter(CLLocationCoordinate2D(latitude: 41.91, longitude: 12.49), zoomLevel: 9, animated: false)
        //following option crashes the emulator
        if CLLocationManager.authorizationStatus() == .authorizedWhenInUse || CLLocationManager.authorizationStatus() == .authorizedAlways {
            mapView.showsUserLocation = true
            mapView.userTrackingMode = .follow
        }
        
        
        //add markers
        let places = utils.createPOImarkers()
        
        mapView.addAnnotations(places)
        let araPacis = MGLPointAnnotation()
        araPacis.coordinate = CLLocationCoordinate2D(latitude: 41.90613, longitude: 12.47546)
        araPacis.title = "Ara Pacis"
        
        let mausoleumAugustum = MGLPointAnnotation()
        mausoleumAugustum.coordinate = CLLocationCoordinate2D(latitude: 41.90602, longitude: 12.47642)
        mausoleumAugustum.title = "Mausoleum van Augustus"
        
        mapView.addAnnotations([araPacis, mausoleumAugustum])
        //hotel
        let hotel = HotelMarker()
        hotel.coordinate = CLLocationCoordinate2D(latitude: 41.901784, longitude: 12.503951)
        hotel.title = "Hotel"
        hotel.subtitle = "Via Marsala 96 Roma, Italië"
        mapView.addAnnotation(hotel)
        
        
        for (_, marker) in userMarkers {
            mapView!.addAnnotation(marker)
        }
        
        view.addSubview(mapView!)
        
        
        registerLocationListener()
        utils.requestCachedLocations()
        
        //Start updating times
        DispatchQueue.main.asyncAfter(deadline: .now() + 20.0) { [weak self] in
            self?.updateTimes()
        }
    }
    
    
    // MARK: add user to map
    func registerLocationListener(){
        let target = SwiftEventBus.self
        SwiftEventBus.onMainThread(target, name:"Location") { result in
            let messageObject = result?.object as! RomereisLocation
            
            let preferences = UserDefaults.standard
            let username = preferences.string(forKey: "username")
            
            if messageObject.user != username {
                self.addUserToMap(location: messageObject)
            }
        }
    }
    
    func addUserToMap(location: RomereisLocation){
        let user = location.user
        let time = location.time
        let lat = location.lat
        let lon = location.lon
        
        // default ISO8601 formatter didn't work
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSZZZZZ"
        
        if userMarkers[user] == nil {
            let marker = UserMarker()
            marker.coordinate = CLLocationCoordinate2D(latitude: lat, longitude: lon)
            marker.title = user
            marker.originalTime = dateFormatter.date(from: time) ?? Date()
            marker.subtitle = marker.originalTime.timeAgo()
            userMarkers[user] = marker
            if mapView != nil {
                mapView.addAnnotation(marker)
            }
        }
        else {
            //the user is already present on the map
            let marker = userMarkers[user]
            marker!.coordinate = CLLocationCoordinate2D(latitude: lat, longitude: lon)
            marker!.originalTime = dateFormatter.date(from: time) ?? Date()
            marker!.subtitle = marker!.originalTime.timeAgo()
        }
    }
    
    private func updateTimes() {
        // put your code here
        for (_, marker) in userMarkers {
            marker.subtitle = marker.originalTime.timeAgo()
        }
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 20.0) { [weak self] in
            self?.updateTimes()
        }
    }

    // MARK: overrides
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        configureView()
    }
    
    override func willTransition(to newCollection: UITraitCollection, with coordinator: UIViewControllerTransitionCoordinator) {
        var url: URL!
        if newCollection.userInterfaceStyle == .dark{
            url = URL(string: "mapbox://styles/mapbox/dark-v10")
        }
        else {
            url = URL(string: "mapbox://styles/mapbox/streets-v11")
        }
        mapView.styleURL = url
    }
    
    
    // MARK: mapView delegates
        // This delegate method is where you tell the map to load a view for a specific annotation based on the willUseImage property of the custom subclass.
    func mapView(_ mapView: MGLMapView, viewFor annotation: MGLAnnotation) -> MGLAnnotationView? {
        return nil
    }
    
    // This delegate method is where you tell the map to load an image for a specific annotation based on the willUseImage property of the custom subclass.
    func mapView(_ mapView: MGLMapView, imageFor annotation: MGLAnnotation) -> MGLAnnotationImage? {
        
        if annotation is UserMarker {
            // For better performance, always try to reuse existing annotations.
            var annotationImage = mapView.dequeueReusableAnnotationImage(withIdentifier: "User")
            
            // If there is no reusable annotation image available, initialize a new one.
            if(annotationImage == nil) {
                annotationImage = MGLAnnotationImage(image: UIImage(named: "User")!, reuseIdentifier: "User")
            }
            
            return annotationImage
        }
        else if annotation is HotelMarker {
            // For better performance, always try to reuse existing annotations.
            var annotationImage = mapView.dequeueReusableAnnotationImage(withIdentifier: "Hotel")
            
            // If there is no reusable annotation image available, initialize a new one.
            if(annotationImage == nil) {
                annotationImage = MGLAnnotationImage(image: UIImage(named: "Hotel")!, reuseIdentifier: "Hotel")
            }
            
            return annotationImage
        }
        else {
            return nil
        }
        
        
    }
    
    func mapView(_ mapView: MGLMapView, rightCalloutAccessoryViewFor annotation: MGLAnnotation) -> UIView? {
        if annotation is BuildingMarker {
            return UIButton(type: .detailDisclosure)
        }
        return nil
    }
    
    
    func mapView(_ mapView: MGLMapView, annotationCanShowCallout annotation: MGLAnnotation) -> Bool {
        // Always allow callouts to popup when annotations are tapped.
        return true
    }
    
    func mapView(_ mapView: MGLMapView, annotation: MGLAnnotation, calloutAccessoryControlTapped control: UIControl) {
        // Hide the callout view.
        mapView.deselectAnnotation(annotation, animated: false)
        
        if let buildingmarker: BuildingMarker = annotation as? BuildingMarker {
            let controller = storyboard!.instantiateViewController(withIdentifier: "MapDetailViewController") as! MapDetailViewController
            controller.itemTitle = buildingmarker.title!
            controller.imagePaths = buildingmarker.images
            controller.contentFilename = buildingmarker.contentFile
            
            let nC = UINavigationController(rootViewController: controller)
            nC.modalPresentationStyle = .fullScreen
            self.present(nC, animated: true, completion: nil)
        }
    }
    
    func onTapCancel(){
        
    }

}

// MGLPointAnnotation subclass
class UserMarker: MGLPointAnnotation {
    var originalTime: Date = Date()
}

class HotelMarker: MGLPointAnnotation {}
class BuildingMarker: MGLPointAnnotation {
    var images: [String]? = nil
    var contentFile: String = "geotracker/verhaaltjes/lorumipsum.html"
}
