//
//  PhotoboardController.swift
//  dummy
//
//  Created by TooMuchRAM on 14/12/2019.
//  Copyright © 2019 TooMuchRAM. All rights reserved.
//

import UIKit

private let reuseIdentifier = "Cell"

class PhotoboardController: UICollectionViewController, UICollectionViewDelegateFlowLayout, UIGestureRecognizerDelegate {
    
    let inset: CGFloat = 10
    let minimumLineSpacing: CGFloat = 10
    let minimumInteritemSpacing: CGFloat = 10
    let cellsPerRow = 2
    
    var cellWidth: CGFloat = 100
    
    var photoboardJSON: [RomereisChatMessage]? {
        didSet {
            
        }
    }

    override func viewDidLoad() {
        if photoboardJSON == nil {
            return
        }
        let marginsAndInsets = inset * 2 + collectionView.safeAreaInsets.left + collectionView.safeAreaInsets.right + minimumInteritemSpacing * CGFloat(cellsPerRow - 1)
        cellWidth = ((collectionView.bounds.size.width - marginsAndInsets) / CGFloat(cellsPerRow)).rounded(.down)
        super.viewDidLoad()

        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false]
        
        // Register cell classes
        self.collectionView!.register(UICollectionViewCell.self, forCellWithReuseIdentifier: reuseIdentifier)

        // Do any additional setup after loading the view.
        
        // Long Press Gesture Recogniser
        let lpgr : UILongPressGestureRecognizer = UILongPressGestureRecognizer(target: self, action: #selector(handleLongPress(gestureRecognizer:)))
        lpgr.minimumPressDuration = 0.3
        lpgr.delegate = self
        lpgr.delaysTouchesBegan = true
        self.collectionView?.addGestureRecognizer(lpgr)

    }

    @objc func handleLongPress(gestureRecognizer : UILongPressGestureRecognizer){

        if (gestureRecognizer.state != UIGestureRecognizer.State.ended){
            return
        }

        let p = gestureRecognizer.location(in: self.collectionView)

        if let indexPath = self.collectionView?.indexPathForItem(at: p){
            let image = (collectionView.cellForItem(at: indexPath)?.backgroundView as! UIImageView).image!
            let placeholder = UIImage(named: "Loading")
            if image == placeholder {
                //Stop executing if the placeholder image is visible
                return
            }
            
            let items = [image]
            let ac = UIActivityViewController(activityItems: items, applicationActivities: nil)
            
            present(ac, animated: true)
        }

    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using [segue destinationViewController].
        // Pass the selected object to the new view controller.
    }
    */

    // MARK: UICollectionViewDataSource

    override func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }


    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return photoboardJSON!.count
    }

    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: reuseIdentifier, for: indexPath)
        
        let placeholder = UIImage(named: "Loading")
        let imageView = UIImageView(image: placeholder)
        imageView.contentMode = .scaleAspectFit
        //                if #available(iOS 13.0, *) {
        //                    cell.backgroundColor = .systemGray4
        //                } else {
        //                    // Fallback on earlier versions
        //                    cell.backgroundColor = .systemGray
        //                }
        cell.backgroundView = imageView
        imageView.imageFromServerURL(photoboardJSON![indexPath[1]].imgpath)
        
        //cell.layer.masksToBounds = true;
        //cell.layer.cornerRadius = cellWidth * (10/57);
        
        // Configure the cell
    
        return cell
    }
    
    

    // MARK: UICollectionViewDelegate

    /*
    // Uncomment this method to specify if the specified item should be highlighted during tracking
    override func collectionView(_ collectionView: UICollectionView, shouldHighlightItemAt indexPath: IndexPath) -> Bool {
        return true
    }
    */

    /*
    // Uncomment this method to specify if the specified item should be selected
    override func collectionView(_ collectionView: UICollectionView, shouldSelectItemAt indexPath: IndexPath) -> Bool {
        return true
    }
    */

    /*
    // Uncomment these methods to specify if an action menu should be displayed for the specified item, and react to actions performed on the item
    override func collectionView(_ collectionView: UICollectionView, shouldShowMenuForItemAt indexPath: IndexPath) -> Bool {
        return false
    }

    override func collectionView(_ collectionView: UICollectionView, canPerformAction action: Selector, forItemAt indexPath: IndexPath, withSender sender: Any?) -> Bool {
        return false
    }

    override func collectionView(_ collectionView: UICollectionView, performAction action: Selector, forItemAt indexPath: IndexPath, withSender sender: Any?) {
    
    }
    */
    
    override func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let image = (collectionView.cellForItem(at: indexPath)?.backgroundView as! UIImageView).image
        let placeholder = UIImage(named: "Loading")
        if image == placeholder {
            //Stop executing if the placeholder image is visible
            return
        }
        let controller = storyboard!.instantiateViewController(withIdentifier: "ImageViewController") as! ImageViewController
        controller.image = image
        present(controller, animated: true)
    }
    
    // https://stackoverflow.com/a/41409642/6261972
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        return CGSize(width: cellWidth, height: cellWidth)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top: inset, left: inset, bottom: inset, right: inset)
    }

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return minimumLineSpacing
    }

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return minimumInteritemSpacing
    }

    
}
