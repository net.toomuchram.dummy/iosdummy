//
// Created by TooMuchRAM on 16/10/2019.
// Copyright (c) 2019 TooMuchRAM. All rights reserved.
//

import Foundation

struct AccountUtilities {
    let sessionId: String?
    let username: String?
    let admin: Bool

    init() {
        let preferences = UserDefaults.standard
        sessionId = preferences.string(forKey: "sessionId")
        username = preferences.string(forKey: "username")
        admin = preferences.bool(forKey: "admin")
    }
}