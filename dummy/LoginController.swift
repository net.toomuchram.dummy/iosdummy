//
//  LoginController.swift
//  dummy
//
//  Created by TooMuchRAM on 05/08/2019.
//  Copyright © 2019 TooMuchRAM. All rights reserved.
//

import UIKit
import SwiftEventBus

class LoginController: UIViewController {
    
    @IBOutlet weak var loadinganimation: UIActivityIndicatorView!
    @IBOutlet weak var errormsg: UILabel!
    
    @IBOutlet weak var usernamefield: UITextField!
    @IBOutlet weak var passwordfield: UITextField!
    
    private let appUtils = AppUtilities()
    
    @IBAction func login(_ sender: Any) {
        
        //get the needed variables
        let username = String(usernamefield.text ?? "")
        let password = String(passwordfield.text ?? "")
        
        
        //check the basic stuff
        if username == "" {
            errormsg.text = "Voer een gebruikersnaam in"
            return
        }
        else if password == "" {
            errormsg.text = "Voer een wachtwoord in"
            return
        } else if password.contains("%") { /* If the password contains a percent sign, some daemon is going to break */
            errormsg.text = "Vraag een nieuw wachtwoord"
            return
        }
        
        //the network-related stuff that may take some time starts here
        //so start the loading animation
        loadinganimation.startAnimating()
        
        
        //send a request to the authentication endpoint
        
        let url = URL(string: "https://augustus.romereis-hl.nl/login")
        let params = "username=" + username + "&password=" + password;
        
        var request = URLRequest(url: url!)
        request.httpMethod = "POST"
        request.httpBody = params.data(using: .utf8)
        
        URLSession.shared.dataTask(with: request){ (data: Data?, response: URLResponse?, error: Error?) in
            if error != nil {
                print("Error during request: \(String(describing: error))")
                DispatchQueue.main.async{
                    self.handleError(error: "ERR_UNKNOWN")
                }
                return
            }
            else{
                let responseString = NSString(data: (data ?? nil)!, encoding: String.Encoding.utf8.rawValue) ?? nil
                if let httpResponse = response as? HTTPURLResponse {
                    DispatchQueue.main.async{
                        if httpResponse.statusCode == 200 {
                            self.requestUIHandler(data: String(responseString!))
                        } else {
                            self.handleError(error: String(responseString!))
                        }
                    }
                }
                if responseString != nil {
                    
                }
                else{
                    DispatchQueue.main.async{
                    }
                }
            }
        }.resume()
            
    }
    
    func requestUIHandler(data: String){
        do {
            let loginResponse = try JSONDecoder().decode(LoginResponse.self, from: data.data(using: .utf8)!)
            let preferences = UserDefaults.standard
            preferences.set(loginResponse.username, forKey: "username")
            preferences.set(loginResponse.sessionId, forKey: "sessionId")
            preferences.set(Bool(truncating: loginResponse.admin as NSNumber), forKey: "admin")
            
            loadinganimation.stopAnimating()
            
            //Request a APNs device token
            UIApplication.shared.registerForRemoteNotifications()
            
            //Start the Web Socket
            SwiftEventBus.post("startWS")
            
            let sessionId = loginResponse.sessionId
            // Prefetch the programmes and other things
            appUtils.getAndSaveGeneralInfo(sessionId: sessionId)
            appUtils.getAndSaveProgramme(sessionId: sessionId)
            appUtils.getAndSaveTeams(sessionId: sessionId)
            
            //Dismiss the login screen
            self.navigationController?.popViewController(animated: true)
            self.dismiss(animated: true, completion: nil)
        }
        catch {
            print("Error: \(error)")
        }
    }
    
    func handleError(error: String) {
        switch(error) {
        case "ERR_UNKNOWN": do {
            errormsg.text = "Onbekende fout"
            }
        case "ERR_MISSING_USERNAME": do {
            errormsg.text = "Voer een gebruikersnaam in"
            }
        case "ERR_MISSING_PASSWORD": do {
            errormsg.text = "Voer een wachtwoord in"
            }
        case "ERR_USERNAME_TOO_LONG": do {
            errormsg.text = "De gebruikersnaam is te lang"
            }
        case "ERR_PASSWORD_TOO_LONG": do {
            errormsg.text = "Het wachtwoord is te lang"
            }
        case "ERR_PASSWORD_TOO_SHORT": do {
            errormsg.text = "Het wachtwoord is te kort"
            }
        case "ERR_NO_SUCH_USER": do {
            errormsg.text = "Onbekende gebruikersnaam"
            }
        case "ERR_WRONG_PASSWORD": do {
            errormsg.text = "Fout wachtwoord"
            }
        default:
            errormsg.text = "Onbekende fout"
        }
        loadinganimation.stopAnimating()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        AppUtilities().requestNotifPerms()
       
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
}

struct LoginResponse: Codable {
    let username: String
    let sessionId: String
    let admin: Int
}
