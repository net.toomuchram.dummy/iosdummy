//
//  DetailViewController.swift
//  dummy
//
//  Created by TooMuchRAM on 30/07/2019.
//

import UIKit
import Mapbox
import UIImageColors
import UserNotifications
import ImageScrollView

class DetailViewController: UIViewController, UIScrollViewDelegate {

    // MARK: Properties
    let appUtils = AppUtilities()

    var detailItem: String? {
        didSet {
            // Update the view.
            //configureView()
        }
    }

    var detailItemTitle: String? {
        didSet {
            //configureView()
        }
    }

    var imageScrollView: ImageScrollView? = nil
    var backgroundColor: UIColor? = nil

    @IBOutlet weak var containerView: UIView!

    // MARK: Load desired view

    func configureView() {
        // Update the user interface for the detail item.

        if let detail = detailItem {

            let type = detail.split(separator: ":")[0]

            //HANDLER FOR TYPE - FILE
            if type == "file" {
                let fullfilename = detail.split(separator: ":")[1]
                handleFile(fullfilename: String(fullfilename))
            } else if type == "special" {
                let specialtype = detail.split(separator: ":")[1]
                handleSpecial(specialType: String(specialtype))
            }
        }
        self.title = detailItemTitle
        // This doesn't look cool
        self.navigationController?.navigationBar.prefersLargeTitles = false
        self.navigationController?.navigationItem.largeTitleDisplayMode = .never
    }

    // MARK: initiate child controller
    func initiate(controller: UIViewController, intoView: UIView) {
        addChild(controller)
        controller.view.translatesAutoresizingMaskIntoConstraints = false
        intoView.addSubview(controller.view)

        NSLayoutConstraint.activate([
            controller.view.leadingAnchor.constraint(equalTo: intoView.leadingAnchor, constant: 0),
            controller.view.trailingAnchor.constraint(equalTo: intoView.trailingAnchor, constant: 0),
            controller.view.topAnchor.constraint(equalTo: intoView.topAnchor, constant: 0),
            controller.view.bottomAnchor.constraint(equalTo: intoView.bottomAnchor, constant: 0)
        ])

        controller.didMove(toParent: self)
    }

    // MARK: General file handler function
    //wrapper function for handleHTML and handleIMG
    func handleFile(fullfilename: String) {
        let fileext = NSString(string: String(fullfilename)).pathExtension

        //In case it's HTML, load the text view controller
        if fileext == "html" {
            let controller = storyboard!.instantiateViewController(withIdentifier: "TextViewController") as! TextViewController
            controller.detailItem = detailItem
            initiate(controller: controller, intoView: containerView)
        } else if fileext == "png" || fileext == "jpg" || fileext == "jpeg" {

            let filename = NSString(string: NSString(string: String(fullfilename)).lastPathComponent).deletingPathExtension
            let filedir = NSString(string: String(fullfilename)).deletingLastPathComponent


            let filePath = appUtils.lookupFile(filedir: filedir, filename: filename, fileext: fileext)
            if (filePath != nil) {
                //load image
                guard let img = UIImage(contentsOfFile: filePath!) else {
                    return
                }

                /*let controller = storyboard!.instantiateViewController(withIdentifier: "ImageViewController") as! ImageViewController
                controller.image = img
                initiate(controller: controller)*/
                //add imageScrollView
                addImageView(image: img)
                self.navigationController?.hidesBarsOnTap = true;
            }
        }
    }

    // MARK: Special handler function
    func handleSpecial(specialType: String) {
        switch specialType {
        case "feed": do {
            let controller = storyboard!.instantiateViewController(withIdentifier: "TextViewController") as! TextViewController
            controller.detailItem = detailItem
            initiate(controller: controller, intoView: containerView)
        }
        case "verzendaankondiging": do {
            let controller = storyboard!.instantiateViewController(withIdentifier: "AnnounceController") as! AnnounceController
            initiate(controller: controller, intoView: containerView)
        }
        case "geotracker": do {
            let controller = storyboard!.instantiateViewController(withIdentifier: "MapController") as! MapController
            initiate(controller: controller, intoView: containerView)
        }
        case "chat": do {
            let controller = storyboard!.instantiateViewController(withIdentifier: "WebViewController") as! WebViewController
            controller.url = "local:assets/chat.html"
            initiate(controller: controller, intoView: containerView)
        }
        case "adminchat": do {
            let controller = storyboard!.instantiateViewController(withIdentifier: "WebViewController") as! WebViewController
            controller.url = "local:assets/chat.html?admin"
            initiate(controller: controller, intoView: containerView)
        }
        case "privacypolicy": do {
            let controller = storyboard!.instantiateViewController(withIdentifier: "WebViewController") as! WebViewController
            controller.url = "remote:romereis-hl.nl/privacy.html"
            initiate(controller: controller, intoView: containerView)
        }
        case "countdown": do {
            let controller = storyboard!.instantiateViewController(withIdentifier: "CountdownController") as! CountdownController
            initiate(controller: controller, intoView: containerView)
        }
        case "photoboard": do {
            loadPhotoboard()
        }
        case "info": do {
            let releaseDate = appUtils.getRomereisDate()
            let now = Date()
            if ((releaseDate as Date) < now) {
                // If the journey has already begun, load the normal info.html file
                let controller = storyboard!.instantiateViewController(withIdentifier: "TextViewController") as! TextViewController
                controller.detailItem = "file:info.html"
                initiate(controller: controller, intoView: containerView)
            } else {
                // If it hasn't, load the info window including the countdown.
                let controller = storyboard!.instantiateViewController(withIdentifier: "InfoWithCountdownController")
                initiate(controller: controller, intoView: containerView)
            }
        }
        case "audio": do {
            let controller = storyboard!.instantiateViewController(withIdentifier: "PlaylistController") as! PlaylistController
            initiate(controller: controller, intoView: containerView)
        }
        default:
            let controller = storyboard!.instantiateViewController(withIdentifier: "TextViewController") as! TextViewController
            controller.detailItem = "file:index.html"
            initiate(controller: controller, intoView: containerView)
        }
    }

    // MARK: load photoboard controller
    func loadPhotoboard() {
        //Create a loading spinner first
        let activityView = UIActivityIndicatorView(style: .whiteLarge)

        activityView.center = view.center
        activityView.startAnimating()
        view.addSubview(activityView)

        let accountUtils = AccountUtilities()
        let sessionId = accountUtils.sessionId
        if sessionId != nil {
            let url = URL(string: "https://augustus.romereis-hl.nl/getMessages?sessionId=\(sessionId!)&type=photoboard")!

            URLSession.shared.dataTask(with: url) { (data, response, error) in
                if data != nil {
                    let responseString = String(data: data!, encoding: .utf8)!
                    if responseString != "ERR_MISSING_SESSIONID" || responseString != "ERR_NO_SUCH_SESSION" || responseString != "ERROR" {
                        DispatchQueue.main.async {
                            let jsonData = (responseString).data(using: String.Encoding(rawValue: String.Encoding.utf8.rawValue))
                            let msgs = try! JSONDecoder().decode([RomereisChatMessage].self, from: jsonData!)
                            let controller = self.storyboard!.instantiateViewController(withIdentifier: "PhotoboardController") as! PhotoboardController
                            controller.photoboardJSON = msgs
                            self.initiate(controller: controller, intoView: self.containerView)
                            activityView.removeFromSuperview()
                        }
                    } else {
                        self.navigationController?.popViewController(animated: true)
                        self.presentLoginScreen()
                    }
                }
            }.resume()
        } else {
            //load login screen
            presentLoginScreen()
        }
    }

    // MARK: Present login screen
    func presentLoginScreen() {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let controller = storyboard.instantiateViewController(withIdentifier: "LoginController")
        controller.modalPresentationStyle = .fullScreen //or .overFullScreen for transparency
        self.present(controller, animated: true, completion: nil)
    }

    // MARK: add image view
    func addImageView(image: UIImage) {
        imageScrollView = ImageScrollView()
        imageScrollView!.frame.size = CGSize(width: view.frame.width, height: view.frame.height)
        view.addSubview(imageScrollView!)
        imageScrollView!.setup()
        imageScrollView!.display(image: image)

        backgroundColor = image.getColors()?.background

        imageScrollView!.backgroundColor = backgroundColor
        imageScrollView!.autoresizingMask = [.flexibleWidth, .flexibleHeight]
    }

    // MARK: overrides
    override func viewDidLoad() {
        super.viewDidLoad()

        configureView()
    }

    override var preferredStatusBarStyle: UIStatusBarStyle {
        if imageScrollView != nil {
            //This whole mathematical fest is meant to determine if a light or a dark statusbar should be shown
            //so it will look good depending on the brightness of the image
            let componentColors = backgroundColor!.cgColor.components

            let colourBrightness0 = componentColors![0] * 299
            let colourBrightness1 = componentColors![1] * 587
            let colourBrightness2 = componentColors![2] * 114

            let totalColourBrightness = colourBrightness0 + colourBrightness1 + colourBrightness2
            let colourBrightness: CGFloat = totalColourBrightness / 1000
            if colourBrightness < 0.5 {
                return .lightContent
            } else {
                if #available(iOS 13.0, *) {
                    return .darkContent
                } else {
                    return .lightContent
                }
            }
        } else {
            // For text, just return light content
            // It will be ignored anyway
            return .lightContent
        }
    }
}


