//
//  RomereisWebsocket.swift
//  dummy
//
//  Created by TooMuchRAM on 30/09/2019.
//  Copyright © 2019 TooMuchRAM. All rights reserved.
//

import Foundation
import UserNotifications
import SwiftEventBus

class RomereisWebsocket {
    
    // MARK: properties
    let ws: WebSocket
    let sessionId: String?
    var loggedIn: Bool = true
    
    // MARK: init()
    init() {
        ws = WebSocket("wss://julius.romereis-hl.nl")
        let preferences = UserDefaults.standard
        sessionId = preferences.string(forKey: "sessionId")
        if sessionId != nil {
            initialiseWS()
            
            let target = SwiftEventBus.self
            SwiftEventBus.onBackgroundThread(target, name:"sendWSmessage") { result in
                self.ws.send(result!.object!)
            }
        }
    }
    
    // MARK: initialise the web socket
    func initialiseWS(){
        ws.event.open = {
            print("Opened WebSocket connection")
            self.ws.send("{\"channel\":\"authentication\", \"sessionId\":\"\(self.sessionId!)\"}")
        }
        ws.event.close = { code, reason, clean in
            print("Disconnected")
            if self.loggedIn {
                print("Trying to reconnect in 15 seconds")
                DispatchQueue.main.asyncAfter(deadline: .now() + .seconds(15), execute: {
                    self.ws.open()
                })
            }
        }
        ws.event.error = { error in
            print("WebSocket error: \(error)")
        }
        ws.event.message = { message in
            if let text = message as? String {
                do {
                    print("Received a message: " + text)
                    self.loggedIn = true
                    let jsonData = (message as! String).data(using: String.Encoding(rawValue: String.Encoding.utf8.rawValue))
                    let obj = try JSONDecoder().decode(RomereisWSmessage.self, from: jsonData!)
                        
                    self.handleMessage(messageObject: obj)
                } catch {
                    print(error)
                }
            }
        }
    }
    
    // MARK: Handle message
    func handleMessage(messageObject: RomereisWSmessage) {
        let channel = messageObject.channel
        
        // MARK: -AUTHENTICATION
        if channel == "authentication" {
            let message = messageObject.message!
            if message == "AUTH_SUCCESS" {
                print("WebSocket Authentication was successful")
            }
        }
            
        // MARK: -MISC
        else if channel == "misc" {
            let message = messageObject.message!
            
            if message == "MULTIPLE_DEVICES" {
                let content = UNMutableNotificationContent()
                content.title = "Meerdere apparaten"
                content.body = "Er zijn meerdere apparaten vanaf dit account verbonden. Het kan zijn dat de app hier niet goed mee omgaat."
                AppUtilities().sendNotificationNow(content: content)
            }
        }
            
        // MARK: -ERRORS
        else if channel == "errors" {
            let error = messageObject.error!
            if error == "ERR_NO_SUCH_SESSION" || error == "ERR_NO_SUCH_USER" {
                self.loggedIn = false
            }
        }
        
        // MARK: -LOCATIONS
        else if channel == "locations" {
            let lat = Double(messageObject.lat!)
            let lon = Double(messageObject.lon!)
            let user = messageObject.user!
            let time = messageObject.time!
            
            let receivedlocation = RomereisLocation(user: user, time: time, lat: lat, lon: lon)
            
            SwiftEventBus.post("Location", sender: receivedlocation)
            
        }
        // MARK: -CHAT
        else if channel == "msg" || channel == "adminmsg" {
            if (
                messageObject.id != nil &&
                messageObject.message != nil &&
                messageObject.imgpath != nil &&
                messageObject.user != nil &&
                messageObject.timestamp != nil
                ){
                let id = messageObject.id!
                let message = messageObject.message!
                let user = messageObject.user!
                let timestamp = messageObject.timestamp!
                let imgpath = messageObject.imgpath!
                let chatmessage = RomereisChatMessage(id: id, message: message, user: user, imgpath: imgpath, timestamp:timestamp)
                if channel == "msg" {
                    SwiftEventBus.post("Chat", sender: chatmessage)
                } else if channel == "adminmsg" {
                    SwiftEventBus.post("Adminchat", sender: chatmessage)
                }
            }
        }
    }
    
    // MARK: disconnect
    func disconnect() {
        ws.close()
    }
    
    // MARK: reconnect
    func reconnect() {
        ws.close()
        ws.open()
    }
}

struct RomereisWSmessage: Codable {
    let channel: String
    let message: String?
    let user: String?
    let time: String?
    let error: String?
    //locations
    let lat: Double?
    let lon: Double?
    //chat
    let id: Int?
    let imgpath: String?
    let timestamp: String?
}

struct RomereisLocation {
    let user: String
    let time: String
    let lat: Double
    let lon: Double
}

struct RomereisChatMessage: Decodable {
    let id: Int
    let message: String
    let user: String
    let imgpath: String
    let timestamp: String
}
