//
//  AppUtilities.swift
//  dummy
//
//  Created by TooMuchRAM on 07/08/2019.
//  Copyright © 2019 TooMuchRAM. All rights reserved.
//

import Foundation
import UserNotifications
import CoreLocation
import SwiftLocation
import SwiftEventBus

class AppUtilities {

    // MARK: request notification permissions
    func requestNotifPerms() {
        let center = UNUserNotificationCenter.current()
        // Request permission to display alerts and play sounds.
        center.requestAuthorization(options: [.alert, .sound, .badge]) { (granted, error) in
            if !granted {
                print("No notification permissions. The user will not receive announcements.")
            }
        }
    }

    // MARK: send notification now
    func sendNotificationNow(content: UNMutableNotificationContent) {
        // Create the request
        let uuidString = UUID().uuidString
        let request = UNNotificationRequest(identifier: uuidString,
                content: content, trigger: nil)

        // Schedule the request with the system.
        let notificationCenter = UNUserNotificationCenter.current()
        notificationCenter.add(request) { (error) in
            if error != nil {
                print("Error upon sending notification: \(error!)")
            }
        }
    }

    // MARK: Send location to server
    func sendLocationToServer(location: CLLocation) {
        print("Sending location to server...")

        let preferences = UserDefaults.standard
        let sessionId = preferences.string(forKey: "sessionId")
        if sessionId != nil {
            var params = "sessionId=" + sessionId!
            params += "&lat=" + String(location.coordinate.latitude)
            params += "&lon=" + String(location.coordinate.longitude)
            let url = URL(string: "https://augustus.romereis-hl.nl/postLocation")

            var request = URLRequest(url: url!)
            request.httpMethod = "POST"
            request.httpBody = params.data(using: .utf8)

            URLSession.shared.dataTask(with: request) { (data: Data?, response: URLResponse?, error: Error?) in
                if error != nil {
                    print("Error during location request: \(String(describing: error))")
                } else {

                    let responseString = NSString(data: (data ?? nil)!, encoding: String.Encoding.utf8.rawValue) ?? nil
                    if responseString != nil && String(responseString!) != "SUCCESS" {
                        print("Server response was \(String(responseString!))")
                    }
                }
            }.resume()
        }
    }

    func handleLocationResult(result: Result<CLLocation, LocationManager.ErrorReason>, method: String = "regular") {
        switch result {
        case .failure(let error):
            debugPrint("Received error: \(error)")
        case .success(let location):
            let preferences = UserDefaults.standard
            let lastlocation_lat = preferences.double(forKey: "lastlocation_lat")
            let lastlocation_lon = preferences.double(forKey: "lastlocation_lon")
            if location.coordinate.latitude != lastlocation_lat && location.coordinate.longitude != lastlocation_lon {
                preferences.set(location.coordinate.latitude, forKey: "lastlocation_lat")
                preferences.set(location.coordinate.longitude, forKey: "lastlocation_lon")

                if method == "regular" {
                    AppUtilities().sendLocationToServer(location: location)
                } else if method == "WebSockets" {
                    let sessionId = preferences.string(forKey: "sessionId")
                    if sessionId != nil {
                        SwiftEventBus.post("sendWSmessage", sender: "{\"channel\":\"locations\", \"sessionId\":\"\(String(describing: sessionId!))\", \"lat\":\"\(String(location.coordinate.latitude))\", \"lon\": \"\(String(location.coordinate.longitude))\"}")
                    }
                }
            } else {
                print("location is the same, so not sending it to server")
            }
        }
    }

    // MARK: look up file from app bundle
    func lookupFile(filedir: String, filename: String, fileext: String) -> String? {
        //Look up the file path
        guard let filePath = Bundle.main.path(forResource: String(filename), ofType: String(fileext), inDirectory: "assets/" + String(filedir))
                else {
            print("File reading error")
            return nil
        }
        return filePath
    }

    // MARK: save and load files from disk
    func saveHTML(html: String, name: String) {
        let fileManager = FileManager.default
        do {
            let documentDirectory = try fileManager.url(for: .documentDirectory, in: .userDomainMask, appropriateFor: nil, create: false)
            let fileURL = documentDirectory.appendingPathComponent(name)
            try html.data(using: .utf8)!.write(to: fileURL)
        } catch {
            print(error)
        }
    }

    func openHTMLfromDisk(name: String) -> String? {
        do {
            let fileURL = getFileURL(filename: name)
            if fileURL != nil {
                return try String(contentsOf: fileURL!)
            }
            return nil
        } catch {
            print(error)
            return nil
        }
    }

    func getFileURL(filename: String) -> URL? {
        do {
            let fileManager = FileManager.default
            let documentDirectory = try fileManager.url(for: .documentDirectory, in: .userDomainMask, appropriateFor: nil, create: false)
            return documentDirectory.appendingPathComponent(filename)
        } catch {
            print(error)
            return nil
        }
    }

    // MARK: fetch latest programme
    func getPage(sessionId: String, page: String, onComplete: @escaping (_ responseString: String?) -> Void) {
        let url = URL(string: "https://augustus.romereis-hl.nl/dynamicContent?name=" + page + "&sessionId=" + sessionId)

        var request = URLRequest(url: url!)
        request.httpMethod = "GET"

        URLSession.shared.dataTask(with: request) { (data: Data?, response: URLResponse?, error: Error?) in
            if error != nil {
                print("Error during request: \(String(describing: error))")
                onComplete(nil)
            } else {
                let responseString = NSString(data: (data ?? nil)!, encoding: String.Encoding.utf8.rawValue) ?? nil
                if responseString! != "ERR_NO_SUCH_SESSION" || responseString! != "ERR_MISSING_SESSIONID" {
                    onComplete(responseString as String?)
                } else {
                    onComplete(nil)
                }
            }
        }.resume()
    }

    func getAndSaveDymanicContent(contentname: String, sessionId: String) {
        //Save a placeholder if the file doesn't exist
        if openHTMLfromDisk(name: "\(contentname).html") == nil {
            saveHTML(html: "\(contentname).html is nog niet opgehaald van het internet.", name: "\(contentname).html")
        }
        //fetch the latest version
        getPage(sessionId: sessionId, page: contentname, onComplete: { content in
            if content != nil {
                self.saveHTML(html: content!, name: "\(contentname).html")
            }
        })
    }

    func getAndSaveProgramme(sessionId: String) {
        getAndSaveDymanicContent(contentname: "programme", sessionId: sessionId)
    }

    func getAndSaveGeneralInfo(sessionId: String) {
        getAndSaveDymanicContent(contentname: "info", sessionId: sessionId)
    }

    func getAndSaveTeams(sessionId: String) {
        getAndSaveDymanicContent(contentname: "teams", sessionId: sessionId)
    }


    // MARK: check if user is logged in
    func checkUserLoggedIn(sessionId: String?, username: String?, onComplete: @escaping (_ loggedIn: Bool, _ admin: Bool?) -> Void) {

        if sessionId != nil && username != nil {
            let params = "sessionId=" + sessionId!
            let url = URL(string: "https://augustus.romereis-hl.nl/isAdmin?" + params)

            var request = URLRequest(url: url!)
            request.httpMethod = "GET"

            URLSession.shared.dataTask(with: request) { (data: Data?, response: URLResponse?, error: Error?) in
                if error != nil {
                    print("Error during username request: \(String(describing: error))")
                    //The user shouldn't get logged out when they have no internet connection
                    onComplete(true, nil)
                } else {

                    let responseString = NSString(data: (data ?? nil)!, encoding: String.Encoding.utf8.rawValue) ?? nil
                    if responseString != nil, let httpResponse = response as? HTTPURLResponse {
                        if httpResponse.statusCode == 200 {
                            onComplete(true, Bool(String(responseString ?? "false")))
                        } else if responseString == "ERR_NO_SUCH_SESSION" || responseString == "ERR_NO_SUCH_USER" {
                            onComplete(false, nil)
                        }
                    }
                }
            }.resume()
        } else {
            onComplete(false, nil)
        }
    }

    // MARK: send device token to server
    func sendDeviceTokenToServer(token: Data) {
        let preferences = UserDefaults.standard
        let sessionId = preferences.string(forKey: "sessionId")
        if sessionId != nil {
            var params = "sessionId=" + sessionId!
            params += "&token=" + token.hexEncodedString().uppercased()
            let url = URL(string: "https://augustus.romereis-hl.nl/postDeviceToken/iOS")

            var request = URLRequest(url: url!)
            request.httpMethod = "POST"
            request.httpBody = params.data(using: .utf8)

            URLSession.shared.dataTask(with: request) { (data: Data?, response: URLResponse?, error: Error?) in
                if error != nil {
                    print("Error during device token request: \(String(describing: error))")
                } else {

                    let responseString = NSString(data: (data ?? nil)!, encoding: String.Encoding.utf8.rawValue) ?? nil
                    if responseString != nil && String(responseString!) != "SUCCESS" {
                        print("Server response was \(String(responseString!))")
                    }
                }
            }.resume()
        }
    }

    func getRomereisDate() -> NSDate {
        let releaseDateString = "2020-03-07 07:20:00"
        let releaseDateFormatter = DateFormatter()
        releaseDateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        return releaseDateFormatter.date(from: releaseDateString)! as NSDate
    }
}

extension Data {
    struct HexEncodingOptions: OptionSet {
        let rawValue: Int
        static let upperCase = HexEncodingOptions(rawValue: 1 << 0)
    }

    func hexEncodedString(options: HexEncodingOptions = []) -> String {
        let format = options.contains(.upperCase) ? "%02hhX" : "%02hhx"
        return map {
            String(format: format, $0)
        }.joined()
    }
}
