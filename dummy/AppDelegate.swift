//
//  AppDelegate.swift
//  dummy
//
//  Created by TooMuchRAM on 30/07/2019.
//  Copyright © 2019 TooMuchRAM. All rights reserved.
//

import UIKit
import SwiftLocation
import CoreLocation
import SwiftEventBus
import AVFoundation

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate, UISplitViewControllerDelegate {
    

    var window: UIWindow? 
    
    let appUtils = AppUtilities()
    
    let significantLocationReq = LocationManager.shared.locateFromGPS(.significant, accuracy: .block) { result in
        print("Received a new significant location")
        AppUtilities().handleLocationResult(result: result)
    }
    
    var regularLocationReq: Timer? = nil

    var ws: RomereisWebsocket? = nil

    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        
        // If you start monitoring significant location changes and your app is subsequently terminated,
        // the system automatically relaunches the app into the background if a new event arrives.
        // Upon relaunch, you must still subscribe to significant location changes to continue receiving location events.
        print("LaunchOptions:")
        print(launchOptions as Any)
        if launchOptions?[UIApplication.LaunchOptionsKey.location] != nil { //launch service again if is background
            LocationManager.shared.locateFromGPS(.significant, accuracy: .block) { result in
                print("[wakeup] Received a new significant location")
                self.appUtils.handleLocationResult(result: result)
            }
        }
        else {
            registerWSStarter()
            // MARK: check if user is logged in
            let preferences = UserDefaults.standard
            let sessionId = preferences.string(forKey: "sessionId")
            let username = preferences.string(forKey: "username")
            appUtils.checkUserLoggedIn(sessionId: sessionId, username: username, onComplete: { loggedIn, admin in
                if loggedIn && self.ws == nil {
                    //start the web socket
                    self.ws = RomereisWebsocket()
                }
                if loggedIn {
                    //fetch latest programme and general info
                    self.appUtils.getAndSaveProgramme(sessionId: sessionId!)
                    self.appUtils.getAndSaveGeneralInfo(sessionId: sessionId!)
                    self.appUtils.getAndSaveTeams(sessionId: sessionId!)
                    //set the admin value
                    if admin != nil {
                        preferences.set(admin, forKey: "admin")
                    }
                }
                else {
                    preferences.set(nil, forKey: "username")
                    preferences.set(nil, forKey: "sessionId")
                }
            })
        }
        
        // Configure audio session
        // Get the singleton instance.
        let audioSession = AVAudioSession.sharedInstance()
        do {
            // Set the audio session category, mode, and options.
            try audioSession.setCategory(.playback, mode: .default, policy: .longFormAudio, options: [])
        } catch {
            print("Failed to set audio session category.")
        }
        
        let splitViewController = window!.rootViewController as! UISplitViewController
        let navigationController = splitViewController.viewControllers[splitViewController.viewControllers.count-1] as! UINavigationController
        navigationController.topViewController!.navigationItem.leftBarButtonItem = splitViewController.displayModeButtonItem
        splitViewController.delegate = self
        
        // Set UNUserNotificationCenterDelegate
        UNUserNotificationCenter.current().delegate = self
        
        //Clear notification icon
        application.applicationIconBadgeNumber = 0
        
        return true
    }
    
    // This is needed to start the WebSocket from the login controller
    func registerWSStarter() {
        let target = SwiftEventBus.self
        SwiftEventBus.onMainThread(target, name:"startWS") { result in
            if self.ws == nil {
                self.ws = RomereisWebsocket()
            }
        }
    }
    
    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
        
        //Start the significant location service
        significantLocationReq.start()
        
        if ws != nil {
            //set loggedIn to false so it does not reconnect
            ws!.loggedIn = false
            ws!.disconnect()
            ws = nil
        }
        
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
        
        //stop the normal location service
        regularLocationReq?.invalidate()
        regularLocationReq = nil
        
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
        
        if ws == nil {
            ws = RomereisWebsocket()
        }
        else {
            ws!.reconnect()
        }
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
        
        //Pause the significant location service
        significantLocationReq.pause()
        
        //start the normal location service
        regularLocationReq = Timer.scheduledTimer(withTimeInterval: 43.8, repeats: true, block: { timer in
            LocationManager.shared.locateFromGPS(.oneShot, accuracy: .city) { result in
                print("Received a new regular location")
                if self.ws != nil {
                    self.appUtils.handleLocationResult(result: result, method: "WebSockets")
                }
                else {
                    self.appUtils.handleLocationResult(result: result)
                }
            }
        })
        regularLocationReq!.fire()
        
        //Register for remote notifications
        application.registerForRemoteNotifications()
    }

    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }

    // MARK: - Split view

    func splitViewController(_ splitViewController: UISplitViewController, collapseSecondary secondaryViewController:UIViewController, onto primaryViewController:UIViewController) -> Bool {
        guard let secondaryAsNavController = secondaryViewController as? UINavigationController else { return false }
        guard let topAsDetailController = secondaryAsNavController.topViewController as? DetailViewController else { return false }
        if topAsDetailController.detailItem == nil {
            // Return true to indicate that we have handled the collapse by doing nothing; the secondary controller will be discarded.
            return true
        }
        return false
    }
    
    // MARK: - APNs
    func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
        appUtils.sendDeviceTokenToServer(token: deviceToken)
    }
    func application(_ application: UIApplication, didFailToRegisterForRemoteNotificationsWithError error: Error) {
        print("There was an error retrieving the device token for APNs:")
        print(error)
    }
}

// Conform to UNUserNotificationCenterDelegate
extension AppDelegate: UNUserNotificationCenterDelegate {

    func userNotificationCenter(_ center: UNUserNotificationCenter,
           willPresent notification: UNNotification,
           withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void)
    {
        if notification.request.content.userInfo["chat"] != nil {
            // Don't send a notification if we're in the app and a chat notification arrives
            completionHandler([])
        } else {
            completionHandler([.alert, .badge, .sound])
        }
    }

}
